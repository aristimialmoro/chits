<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  Controller for Prequels and Indicators
 *
 *   @author Rose Angelica S. Carandang <rscarandang@up.edu.ph>
 *   @author Jeremiah Sean S. De Guzman <jsdeguzman6@up.edu.ph>
 *   @author Reena Myka De Guzman <[<email address>]>
 *   @author Ced Rick C. Flores <ccflores@up.edu.ph>
 *   @author John Robinson S. Francisco <jsfrancisco5@up.edu.ph>
 *   @author Harold R. Mansilla <hrmansilla08@gmail.com>
 *   @author Virgilio M. Mendoza III <vmmendoza1@up.edu.ph>
 *   @author Waleed C. Occidental <woccidental@up.edu.ph>
 *   @author Sean Louie Pineda <pinedaseanlouie@gmai>
 *   @author Lorenz Timothy Barco Ranera <lbranera@up.edu.ph>
 */

class Data extends CI_Controller {
    public function index()
	{
        $this->update_warehouse();
		$this->load->view('dashboard');
    }


    public function update_warehouse(){
        $this->load->model("data_model");
        //$this->data_model->update_warehouse();
        $this->data_model->update_external_warehouse();
        //redirect('');
    }







    
    /**
     * DISAGGREGATE BY BIRTH CONTROLLER FUNCTIONS ==================================================================
     * 
     * @author Harold R. Mansilla <hrmansilla08@gmail.com>
     */

    public function disaggregated_births($Area="kvhc", $Period="monthly", $Start="2012-01-01", $End="2013-01-01"){
        $data = array('Start' => $Start, 'End' => $End, 'Period', 'Period' => $Period, 'Area' => $Area);
        $this->load->view('disaggregated_births',$data);
    }

    public function get_disaggregated_births(){
        $this->load->model("preq_model");
        if(isset($_REQUEST)){
            $Start = htmlspecialchars($this->input->get('Start'));
            $End = htmlspecialchars($this->input->get('End'));
            $Period = htmlspecialchars($this->input->get('Period'));
            $Area = htmlspecialchars($this->input->get('Area'));
            $this->preq_model->set_all_births_view($Area);
            $res = $this->preq_model->get_disaggregated_births($Start, $End, $Period, $Area);
            echo json_encode($res);
        }
    }

    /** END OF DISAGGREGATE BY BIRTH CONTROLLER FUNCTIONS ========================================================== */
    










    /**
     * POPULATION BY AGE BRACKET CONTROLLER FUNCTIONS ===============================================================
     *
     *   @author Reena Myka De Guzman <[<email address>]>
     */
    public function population_by_age_bracket($Area="kvhc", $Period="monthly", $Start="2012-01-01", $End="2013-01-01"){
        $data = array('Start' => $Start, 'End' => $End, 'Period' => $Period, 'Area' => $Area);
        $this->load->view('population_by_age_bracket',$data);
    }

    public function get_population_by_age_bracket(){
        $this->load->model("preq_model");
        if(isset($_REQUEST)){
            $Start = htmlspecialchars($this->input->get('Start'));
            $End = htmlspecialchars($this->input->get('End'));
            $Period = htmlspecialchars($this->input->get('Period'));
            $Area = htmlspecialchars($this->input->get('Area'));
            $this->preq_model->set_population_by_age_bracket_view($Area);
            $res = $this->preq_model->get_populationbyagebracket2($Start, $End, $Period, $Area);
            echo json_encode($res);
        }
    }

    /** END OF POPULATION BY AGE BRACKET CONTROLLER FUNCTION ====================================================== */









    /**
     * NUMBER OF CONSULATIONS PER HEALTH CENTER CONTROLLER FUNCTIONS =================================================
     *
     * @author Lorenz Timothy Barco Ranera <lbranera@up.edu.ph>
     */
    private function get_consultation_chart_info($warehouse){
        $labels = array("KVHC", "Phase 2 Area 1", "Sipacalmacen", "Tangos");
        $adultin = array();
        $adultre = array();
        $pedsin = array();
        $pedsre = array();
        
        for($i=0; $i<count($warehouse); $i++){
            for($j=0; $j<count($warehouse[$i]); $j++){
               if($j==0){
                 array_push($adultin,$warehouse[$i][$j]['count(*)']);
               }else if($j==1){
                 array_push($adultre,$warehouse[$i][$j]['count(*)']);
               }else if($j==2){
                 array_push($pedsin,$warehouse[$i][$j]['count(*)']);
               }else if($j==3){
                 array_push($pedsre,$warehouse[$i][$j]['count(*)']);
               }
            }
        }

        $chart_info = array("labels" => $labels,
                            "datasets" => array(
                            array("label" => "ADULTINITIAL" , "backgroundColor" => "#039be5", "data" => $adultin),
                            array("label" => "ADULTRETURN"  , "backgroundColor" => "#ffa726", "data" => $adultre),
                            array("label" => "PEDSINITIAL", "backgroundColor" => "#66bb6a", "data" => $pedsin),
                            array("label" => "PEDSRETURN" , "backgroundColor" => "#26a69a", "data" => $pedsre)
                            )
        );


        return $chart_info;
    }
    
    public function consultation(){
        $this->load->model("preq_model");
        $data['consultation'] = $this->preq_model->get_consultation_warehouse();
        $data["chart_info"] = $this->get_consultation_chart_info($data["consultation"]);
        $data["y_axis"] = "No. of Encounters";
        $data["x_axis"] = "Health Center Area";
        $data["title"] = "Number of Consultations per Health Center Area";
        $this->load->view('consultation', $data);
    }

    /** END OF NUMBER OF CONSULATIONS PER HEALTH CENTER CONTROLLER FUNCTIONS ========================================= */
    








  /**
   * BIRTH RATE CONTROLLER FUNCTIONS =================================================================================
   * 
   *   @author Rose Angelica S. Carandang <rscarandang@up.edu.ph>
   */
   
    public function birthrate($area = 'phase2area1', $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        $this->load->model("data_model");

        $area = strtolower($area);
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos", "philippines");

        if(!in_array($area, $areas)){
            show_404();
        }

        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $period = strtolower($period);
        $periods = array("yearly", "quarterly", "monthly");

        if(!in_array($period, $periods)){
            show_404();
        }

        $birthrates = $this->data_model->get_birthrates($area, $start_date, $end_date, $period);

        $updated_data = $this->add_missing_labels_birthrate($birthrates, $start_date, $end_date, $period);
        $labels = $updated_data['labels'];
        $birthrates_data = $updated_data['birthrates_data'];

        $data['title'] = ucfirst($period) . " Crude Birth Rate in " . strtoupper($area);
                              
        if(strcmp($period, "yearly") === 0){
            $data["x_axis_label"] = "Year";
        }else if(strcmp($period, "quarterly") === 0){
            $data["x_axis_label"] = "Quarter, Year";
        }else{
            $data["x_axis_label"] = "Month, Year";
        }

        $data["y_axis_label"] = "Birth Rate (in percentage)";

        //set up json file for chart
        $data['dataset'] = array("labels" => $labels,
                            "datasets" => array(
                                array(
                                    "label" => "Birth Rate" ,
                                    "fill" => false,
                                    "backgroundColor" => "#039be5",
                                    "borderColor" => "#039be5",
                                    "data" => $birthrates_data)
                            )
                        );

        $this->load->view('birth_rate', $data);

    }
    
    public function birth_rate_table($area = 'phase2area1', $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        $this->load->model("data_model");

        $area = strtolower($area);
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos", "philippines");

        if(!in_array($area, $areas)){
            show_404();
        }

        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $period = strtolower($period);
        $periods = array("yearly", "quarterly", "monthly");

        if(!in_array($period, $periods)){
            show_404();
        }

        $birthrates = $this->data_model->get_birthrates($area, $start_date, $end_date, $period);
        $updated_data = $this->add_missing_labels_birthrate($birthrates, $start_date, $end_date, $period);
        $labels = $updated_data['labels'];
        $birthrates_data = $updated_data['birthrates_data'];

        $data['title'] = ucfirst($period) . " Crude Birth Rate in ".strtoupper($area);

        //set up json file for table
        $table_info["columns"] = array(array("title" => ""), array("title" => "Birth Rate"));

        if(strcmp($period, "quarterly") === 0){
            $table_info["columns"][0] = "Quarter";
        }else if(strcmp($period, "monthly")   === 0){
            $table_info["columns"][0] = "Month";
        }else{
            $table_info["columns"][0] = "Year";
        }

        $table_info['data'] = array();

        $size = sizeof($labels);
        for($i = 0; $i < $size; $i++){
            $curr_data[0] = $labels[$i];
            $curr_data[1] = $birthrates_data[$i]."%";
            array_push($table_info['data'], $curr_data);
        }
        
        $data['table_info'] = $table_info;
       $this->load->view('birth_rate_table', $data);

    }

    public function birth_rate_csv($area = 'phase2area1', $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        $this->load->model("data_model");

        $area = strtolower($area);
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos", "philippines");

        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $period = strtolower($period);
        $periods = array("yearly", "quarterly", "monthly");

        $birthrates = $this->data_model->get_birthrates($area, $start_date, $end_date, $period);
        $updated_data = $this->add_missing_labels_birthrate($birthrates, $start_date, $end_date, $period);
        $labels = $updated_data['labels'];
        $birthrates_data = $updated_data['birthrates_data'];

        //set up csv file
        $file_name = "birth_rate.csv";
        $file_content = "";

        $this->load->helper("download");

        $period = "Year";
        if(strcmp($period, "quarterly") === 0){
            $period = "Quarter";
        }else if(strcmp($period, "monthly")   === 0){
            $period = "Month";
        }

        $column_names = array($period, "Birth Rate");

        $col_count = count($column_names);
        $row_count = count($birthrates_data);

        foreach($column_names as $index => $column){
            $file_content .= $column;
            if($index + 1 < $col_count){
                $file_content .= ",";
            }else{
                $file_content .= "\r\n";
            }
        }

        for($i = 0; $i < $row_count; $i++){
            $file_content.= $labels[$i].",".$birthrates_data[$i];
            $file_content .= "\r\n";
        }

        force_download($file_name, $file_content);

    }

    private function add_missing_labels_birthrate($birthrates, $start_date, $end_date, $period){
        $birthrates_data = array();

        if(strcasecmp($period, "yearly") === 0){
            $labels = range(idate("Y", strtotime($start_date)), idate("Y", strtotime($end_date)));
            $row = 0;
            foreach($labels as $year_label){
                if ($row < sizeof($birthrates)) {
                    
                    if($birthrates[$row]['birth_year'] == $year_label){
                        array_push($birthrates_data ,$birthrates[$row]['total']);
                        $row++;
                    }else{
                        array_push($birthrates_data,0);
                    }
                }else{
                    array_push($birthrates_data,0);
                }
            }
        }elseif (strcasecmp($period, "monthly") === 0) {
            $labels = $this->dateRange($start_date, $end_date, '+1 month', "M 'y");
            $row = 0;
            foreach($labels as $month_year_label){
                if ($row < sizeof($birthrates)) {
                    $curr_year_label = '20'.substr($month_year_label, -2);
                    if($curr_year_label > date('Y')) {
                        $curr_year_label = $curr_year_label - 100;
                    }

                    $curr_month_label = idate("m", strtotime(substr($month_year_label, 0, 3)));

                    if($birthrates[$row]['birth_year'] == $curr_year_label && $birthrates[$row]['birth_month'] == $curr_month_label){
                        array_push($birthrates_data,$birthrates[$row]['total']);
                        $row++;
                    }else{
                        array_push($birthrates_data,0);
                    }
                }else{
                    array_push($birthrates_data,0);
                }
            }
        }elseif (strcasecmp($period, "quarterly") === 0) {
            $labels = $this->dateRange($start_date, $end_date, '+1 month', "M 'y");

            //changes the month-year label to quarter-year
            foreach($labels as &$month_year_label){
                $curr_month_label = substr($month_year_label, 0, 3);

                if(strcasecmp($curr_month_label, "jan") === 0 || strcasecmp($curr_month_label, "feb") === 0 || strcasecmp($curr_month_label, "mar") === 0){
                    $month_year_label = str_replace($curr_month_label, "Q1", $month_year_label);
                }elseif (strcasecmp($curr_month_label, "apr") === 0 || strcasecmp($curr_month_label, "may") === 0 || strcasecmp($curr_month_label, "jun") === 0) {
                    $month_year_label = str_replace($curr_month_label, "Q2", $month_year_label);
                }elseif (strcasecmp($curr_month_label, "jul") === 0 || strcasecmp($curr_month_label, "aug") === 0 || strcasecmp($curr_month_label, "sep") === 0) {
                    $month_year_label = str_replace($curr_month_label, "Q3", $month_year_label);
                }elseif (strcasecmp($curr_month_label, "oct") === 0 || strcasecmp($curr_month_label, "nov") === 0 || strcasecmp($curr_month_label, "dec") === 0) {
                    $month_year_label = str_replace($curr_month_label, "Q4", $month_year_label);
                }
            }
            $labels = array_unique($labels);
            $labels = array_values($labels); //compresses the array

            $row = 0;
            foreach($labels as $quarter_year_label){
                if ($row < sizeof($birthrates)) {
                    $quarter_str = substr($quarter_year_label, 1, 1);

                    $curr_year_label = '20'.substr($quarter_year_label, -2);

                    if($curr_year_label > date('Y')) {
                        $curr_year_label = $curr_year_label - 100;
                    }

                    $curr_quarter_label = intval($quarter_str);

                    if($birthrates[$row]['birth_year'] == $curr_year_label && $birthrates[$row]['birth_quarter'] == $curr_quarter_label){
                        array_push($birthrates_data, $birthrates[$row]['total']);
                        $row++;
                    }else{
                        array_push($birthrates_data,0);
                    }
                }else{
                    array_push($birthrates_data,0);
                }
            }
        }

        $updated_data['labels'] = $labels;
        $updated_data['birthrates_data'] = $birthrates_data;

        return $updated_data;
    }
    /** END OF BIRTH RATE CONTROLLER FUNCTIONS ===================================================================== */












    /**
     * NUMBER OF ATTENDED BIRTHS BY SKILLED HEALTH PROFESSIONALS CONTROLLER FUNCTIONS =================================
     * 
     * @author Jeremiah Sean S. De Guzman <jsdeguzman6@up.edu.ph>
     */
    
    public function births($area = "phase2area1", $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        $area = strtolower($area);
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos", "philippines");

        if(!in_array($area, $areas)){
            show_404();
        }

        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $period = strtolower($period);
        $periods = array("yearly", "quarterly", "monthly");

        if(!in_array($period, $periods)){
            show_404();
        }

        $start_date .= ' 00:00:00';
        $end_date .= ' 00:00:00';

        $this->load->model("data_model");

        $data['chart_info']['short_title'] = ucfirst($period) . " Births Attended in " . strtoupper($area);
        $data['chart_info']['long_title'] = ucfirst($period) . " Births Attended by Skilled Professionals in " . strtoupper($area);
        $data["warehouse"] = $this->data_model->get_births_warehouse($area, $start_date, $end_date, $period);

        $data["chart_info"]["data"] = $this->get_births_chart_info($data["warehouse"], $area, $period, $start_date, $end_date);

        if(strcmp($period, "yearly") === 0){
            $data['chart_info']["x_axis"] = "Year";
        }else if(strcmp($period, "quarterly") === 0){
            $data['chart_info']["x_axis"] = "Quarter, Year";
        }else{
            $data['chart_info']["x_axis"] = "Month, Year";
        }

        $data['chart_info']['y_axis'] = "Births Attended";

        $this->load->view('births_attended', $data);
    }

    public function births_table($area = "phase2area1", $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        $area = strtolower($area);
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos", "philippines");

        if(!in_array($area, $areas)){
            show_404();
        }

        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $period = strtolower($period);
        $periods = array("yearly", "quarterly", "monthly");

        if(!in_array($period, $periods)){
            show_404();
        }

        $start_date .= ' 00:00:00';
        $end_date .= ' 00:00:00';

        $this->load->model("data_model");

        $warehouse = $this->data_model->get_births_warehouse($area, $start_date, $end_date, $period);
        
        $data["table_info"] = $this->get_births_table_info($warehouse, $area, $period, $start_date, $end_date);

        $data['title'] = ucfirst($period) . " Births Attended by Skilled Professionals in " . strtoupper($area);

        $this->load->view('births_attended_table', $data);
    }

    public function births_table_csv($area = "phase2area1", $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        $area = strtolower($area);
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos", "philippines");

        if(!in_array($area, $areas)){
            show_404();
        }

        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $period = strtolower($period);
        $periods = array("yearly", "quarterly", "monthly");

        if(!in_array($period, $periods)){
            show_404();
        }

        $start_date .= ' 00:00:00';
        $end_date .= ' 00:00:00';

        $this->load->model("data_model");

        $warehouse = $this->data_model->get_births_warehouse($area, $start_date, $end_date, $period);
        
        $table_info = $this->get_births_table_info($warehouse, $area, $period, $start_date, $end_date);

        $file_name = strtolower($period) . "-births-attended-by-skilled-professionals-in-" . strtolower($area);

        $this->load->helper("download");

        $file_content = "";
        $col_count = count($table_info["columns"]);
        $row_count = count($table_info["data"]);

        foreach($table_info["columns"] as $index => $column){
            $file_content .= $column["title"];
            if($index + 1 < $col_count){
                $file_content .= ",";
            }else{
                $file_content .= "\r\n";
            }
        }

        foreach($table_info["data"] as $row_index => $row){
            foreach($row as $col_index => $cell){
                $file_content .= $cell;
                if($col_index + 1 < $col_count){
                    $file_content .= ",";
                }else if($row_index + 1 < $row_count){
                    $file_content .= "\r\n";
                }
            }
        }

        force_download($file_name, $file_content);        
    }

    private function get_births_chart_info($warehouse, $area, $period, $start_date, $end_date){
        $chart_info = array("labels" => array(),
                            "datasets" => array(array("label" => "Doctors" , "backgroundColor" => "#039be5", "data" => array()),
                                                array("label" => "Midwives"  , "backgroundColor" => "#ffa726", "data" => array()),
                                                array("label" => "Nurses", "backgroundColor" => "#66bb6a", "data" => array())
                                               )
                           );

        $start_year = date("Y", strtotime($start_date));
        $end_year = date("Y", strtotime($end_date));
        $index = 0;
        $warehouse_size = count($warehouse);

        $year = $start_year;

        $is_quarterly = strcmp($period, "quarterly") === 0;
        $is_monthly   = strcmp($period, "monthly")   === 0;

        for(; $year <= $end_year; $year++){
            if($is_quarterly){
                $counter     = $year == $start_year ? ceil(date("n", strtotime($start_date))/3) : 1;
                $counter_end = $year == $end_year   ? ceil(date("n", strtotime($end_date))/3)   : 4;
            }else if($is_monthly){
                $counter     = $year == $start_year ? (int) date("n", strtotime($start_date)) : 1 ;
                $counter_end = $year == $end_year   ? (int) date("n", strtotime($end_date))   : 12;
            }else{
                $counter = $counter_end = 1;
            }
            for(; $counter <= $counter_end; $counter++){
                if($is_quarterly){
                    $chart_info["labels"][] = "Q" . $counter . " '" . str_pad($year % 100, 2, '0', STR_PAD_LEFT);
                }else if($is_monthly){
                    $chart_info["labels"][] = date('M', mktime(0, 0, 0, $counter)) . " '" .
                                                str_pad($year % 100, 2, '0', STR_PAD_LEFT);
                }else{
                    $chart_info["labels"][] = $year;
                }
                if($index < $warehouse_size){
                    $row = $warehouse[$index];
                    $use_row = $row["birth_year"] == $year;
                    if($is_quarterly){
                        $use_row &= $row["birth_quarter"] == $counter;
                    } else if($is_monthly){
                        $use_row &= $row["birth_month"] == $counter;
                    }
                    if($use_row){
                        $chart_info["datasets"][0]["data"][] = (int) $row["Doctors"];
                        $chart_info["datasets"][1]["data"][] = (int) $row["Midwives"];
                        $chart_info["datasets"][2]["data"][] = (int) $row["Nurses"];
                        $index++;
                    }else{
                        $chart_info["datasets"][0]["data"][] = 0;
                        $chart_info["datasets"][1]["data"][] = 0;
                        $chart_info["datasets"][2]["data"][] = 0;
                    }
                }else{
                    $chart_info["datasets"][0]["data"][] = 0;
                    $chart_info["datasets"][1]["data"][] = 0;
                    $chart_info["datasets"][2]["data"][] = 0;
                }
            }
        }
        return $chart_info;
    }

    private function get_births_table_info($warehouse, $area, $period, $start_date, $end_date){
        $table_info = array("columns" => array(), "data" => array());

        $table_info["columns"] = array(array("title" => ""), array("title" => "Doctors"), 
                                      array("title" => "Midwives"), array("title" => "Nurses"));

        $start_year = date("Y", strtotime($start_date));
        $end_year = date("Y", strtotime($end_date));
        $index = 0;
        $warehouse_size = count($warehouse);

        $year = $start_year;

        $is_quarterly = strcmp($period, "quarterly") === 0;
        $is_monthly   = strcmp($period, "monthly")   === 0;

        if($is_quarterly){
            $table_info["columns"][0]["title"] = "Quarter";
        }else if($is_monthly){
            $table_info["columns"][0]["title"] = "Month";
        }else{
            $table_info["columns"][0]["title"] = "Year";
        }
        
        for(; $year <= $end_year; $year++){
            if($is_quarterly){
                $counter     = $year == $start_year ? ceil(date("n", strtotime($start_date))/3) : 1;
                $counter_end = $year == $end_year   ? ceil(date("n", strtotime($end_date))/3)   : 4;
            }else if($is_monthly){
                $counter     = $year == $start_year ? (int) date("n", strtotime($start_date)) : 1 ;
                $counter_end = $year == $end_year   ? (int) date("n", strtotime($end_date))   : 12;
            }else{
                $counter = $counter_end = 1;
            }
            for(; $counter <= $counter_end; $counter++){
                $new_row = array();
                if($is_quarterly){
                    $new_row[] = "Q" . $counter . " '" . str_pad($year % 100, 2, '0', STR_PAD_LEFT);
                }else if($is_monthly){
                    $new_row[] = date('M', mktime(0, 0, 0, $counter)) . " '" .
                                                str_pad($year % 100, 2, '0', STR_PAD_LEFT);
                }else{
                    $new_row[] = $year;
                }
                if($index < $warehouse_size){
                    $row = $warehouse[$index];
                    $use_row = $row["birth_year"] == $year;
                    if($is_quarterly){
                        $use_row &= $row["birth_quarter"] == $counter;
                    } else if($is_monthly){
                        $use_row &= $row["birth_month"] == $counter;
                    }
                    if($use_row){
                        $new_row[] = (int) $row["Doctors"];
                        $new_row[] = (int) $row["Midwives"];
                        $new_row[] = (int) $row["Nurses"];
                        $index++;
                    }else{
                        $new_row[] = 0;
                        $new_row[] = 0;
                        $new_row[] = 0;
                    }
                }else{
                    $new_row[] = 0;
                    $new_row[] = 0;
                    $new_row[] = 0;
                }
                $table_info["data"][] = $new_row;
            }
        }
        return $table_info;
    }
    /** END OF NUMBER OF ATTENDED BIRTHS BY SKILLED HEALTH PROFESSIONALS CONTROLLER FUNCTIONS ======================= */











    /**
     * CONTRACEPTIVE AND FAMILY PLANNING USERS: STATUS CONTROLLER FUNCTIONS ==========================================
     * 
     * @author Ced Rick C. Flores <ccflores@up.edu.ph>
     */
    
    public function contraceptive($area="kvhc", $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $this->load->model("data_model");
        $warehouse = $this->data_model->get_family_planning_status($area, $start_date, $end_date, $period);

        $data['cu_count'] = array();
        $data['cc_count'] = array();
        $data['rs_count'] = array();
        $data['cm_count'] = array();
        $data['lu_count'] = array();
        $data['na_count'] = array();

        $data['y_axis_label'] = "Number of Users";

        if(strcasecmp($period, "yearly") === 0){
            $data['labels'] = range(idate("Y", strtotime($start_date)), idate("Y", strtotime($end_date)));
            $data['x_axis_label'] = "Year";
            $row = 0;
            foreach($data['labels'] as $curr_year){
                if ($row < sizeof($warehouse)) {
                    if($warehouse[$row]['year'] == $curr_year){
                        array_push($data['cu_count'],$warehouse[$row]['cu_count']);
                        array_push($data['cc_count'],$warehouse[$row]['cc_count']);
                        array_push($data['rs_count'],$warehouse[$row]['rs_count']);
                        array_push($data['cm_count'],$warehouse[$row]['cm_count']);
                        array_push($data['lu_count'],$warehouse[$row]['lu_count']);
                        array_push($data['na_count'],$warehouse[$row]['na_count']);
                        $row++;
                    }else{
                        array_push($data['cu_count'],0);
                        array_push($data['cc_count'],0);
                        array_push($data['rs_count'],0);
                        array_push($data['cm_count'],0);
                        array_push($data['lu_count'],0);
                        array_push($data['na_count'],0);
                    }
                }
            }
        }elseif (strcasecmp($period, "monthly") === 0) {
            $data['labels'] = $this->dateRange($start_date, $end_date, '+1 month', "M 'y");
            $data['x_axis_label'] = "Month";
            $row = 0;
            foreach($data['labels'] as $curr_month_year){
                if ($row < sizeof($warehouse)) {
                    $curr_year_str = substr($curr_month_year, -2);
                    $curr_month_str = substr($curr_month_year, 0, 3);
                    $curr_year = '20'.$curr_year_str;
                    if($curr_year > date('Y')) {
                        $curr_year = $curr_year - 100;
                    }
                    $curr_month = idate("m", strtotime($curr_month_str));
                    if($warehouse[$row]['year'] == $curr_year && $warehouse[$row]['month'] == $curr_month){
                        array_push($data['cu_count'],$warehouse[$row]['cu_count']);
                        array_push($data['cc_count'],$warehouse[$row]['cc_count']);
                        array_push($data['rs_count'],$warehouse[$row]['rs_count']);
                        array_push($data['cm_count'],$warehouse[$row]['cm_count']);
                        array_push($data['lu_count'],$warehouse[$row]['lu_count']);
                        array_push($data['na_count'],$warehouse[$row]['na_count']);
                        $row++;
                    }else{
                        array_push($data['cu_count'],0);
                        array_push($data['cc_count'],0);
                        array_push($data['rs_count'],0);
                        array_push($data['cm_count'],0);
                        array_push($data['lu_count'],0);
                        array_push($data['na_count'],0);
                    }
                }
            }
        }elseif (strcasecmp($period, "quarterly") === 0) {
            $data['labels'] = $this->dateRange($start_date, $end_date, '+1 month', "M 'y");
            $data['x_axis_label'] = "Quarter";
            foreach($data['labels'] as &$month_year){
                $month = substr($month_year, 0, 3);
                if(strcasecmp($month, "jan") === 0 || strcasecmp($month, "feb") === 0 || strcasecmp($month, "mar") === 0){
                    $month_year = str_replace($month, "Q1", $month_year);
                }elseif (strcasecmp($month, "apr") === 0 || strcasecmp($month, "may") === 0 || strcasecmp($month, "jun") === 0) {
                    $month_year = str_replace($month, "Q2", $month_year);
                }elseif (strcasecmp($month, "jul") === 0 || strcasecmp($month, "aug") === 0 || strcasecmp($month, "sep") === 0) {
                    $month_year = str_replace($month, "Q3", $month_year);
                }elseif (strcasecmp($month, "oct") === 0 || strcasecmp($month, "nov") === 0 || strcasecmp($month, "dec") === 0) {
                    $month_year = str_replace($month, "Q4", $month_year);
                }
            }
            $data['labels'] = array_unique($data['labels']);
            $data['labels'] = array_values($data['labels']); //compresses the array

            $row = 0;
            foreach($data['labels'] as $curr_quarter_year){
                if ($row < sizeof($warehouse)) {
                    $curr_year_str = substr($curr_quarter_year, -2);
                    $curr_quarter_str = substr($curr_quarter_year, 1, 1);
                    $curr_year = '20'.$curr_year_str;
                    if($curr_year > date('Y')) {
                        $curr_year = $curr_year - 100;
                    }
                    $curr_quarter = intval($curr_quarter_str);
                    if($warehouse[$row]['year'] == $curr_year && $warehouse[$row]['quarter'] == $curr_quarter){
                        array_push($data['cu_count'],$warehouse[$row]['cu_count']);
                        array_push($data['cc_count'],$warehouse[$row]['cc_count']);
                        array_push($data['rs_count'],$warehouse[$row]['rs_count']);
                        array_push($data['cm_count'],$warehouse[$row]['cm_count']);
                        array_push($data['lu_count'],$warehouse[$row]['lu_count']);
                        array_push($data['na_count'],$warehouse[$row]['na_count']);
                        $row++;
                    }else{
                        array_push($data['cu_count'],0);
                        array_push($data['cc_count'],0);
                        array_push($data['rs_count'],0);
                        array_push($data['cm_count'],0);
                        array_push($data['lu_count'],0);
                        array_push($data['na_count'],0);
                    }
                }
            }
        }

        $data['chart_info'] = array("labels" => $data['labels'],
                            "datasets" => array(array("label" => "Current/Continuing User (CU)", "backgroundColor" => "#039be5", "data" => $data['cu_count']),
                                                array("label" => "Changing Clinic (CC) 	", "backgroundColor" => "#ffa726", "data" => $data['cc_count']),
                                                array("label" => "Restart (RS)", "backgroundColor" => "#66bb6a", "data" => $data['rs_count']),
                                                array("label" => "Changing Method (CM)", "backgroundColor" => "#26a69a", "data" => $data['cm_count']),
                                                array("label" => "Learning User (LU)", "backgroundColor" => "#ffca28", "data" => $data['lu_count']),
                                                array("label" => "New Acceptor (NA)", "backgroundColor" => "#ab47bc", "data" => $data['na_count'])
                                               )
                           );

        $this->load->view('family_planning_status', $data);
    }

    public function contraceptive_table($area = 'phase2area1', $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        $this->load->model("data_model");

        $area = strtolower($area);
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos", "philippines");

        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $period = strtolower($period);
        $periods = array("yearly", "quarterly", "monthly");

        $family_planning_status = $this->data_model->get_family_planning_status($area, $start_date, $end_date, $period);
        $updated_data = $this->add_missing_labels_contraceptive($family_planning_status, $start_date, $end_date, $period);
        //set up json file for table
        $table = array();
        $columns = array();

        $column_names = array(
            $updated_data['x_axis_label'],
            "Current/Continuing User (CU)",
            "Changing Clinic (CC)",
            "Restart (RS)",
            "Changing Method (CM)",
            "Learning User (LU)",
            "New Acceptor (NA)"
            );

        for($i =0; $i<7; $i++){
           $columns[$i] = array("title" => $column_names[$i]);
        }

        $data['updated_data'] = $updated_data;

        for($i = 0; $i < sizeof($updated_data['cu_count']); $i++){
            $row = array();
            array_push($row, $updated_data['labels'][$i]);
            array_push($row, $updated_data['cu_count'][$i]);
            array_push($row, $updated_data['cc_count'][$i]);
            array_push($row, $updated_data['rs_count'][$i]);
            array_push($row, $updated_data['cm_count'][$i]);
            array_push($row, $updated_data['lu_count'][$i]);
            array_push($row, $updated_data['na_count'][$i]);
            array_push($table, $row);
        }
        
        $table_info = array("columns" => $columns,
                            "data" => $table
                            );

        $data['table_info'] = $table_info;
        $this->load->view('family_planning_status_table', $data);

    }

    public function contraceptive_csv($area = "phase2area1", $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        $this->load->model("data_model");

        $area = strtolower($area);
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos", "philippines");

        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $period = strtolower($period);
        $periods = array("yearly", "quarterly", "monthly");

        $family_planning_status = $this->data_model->get_family_planning_status($area, $start_date, $end_date, $period);
        $updated_data = $this->add_missing_labels_contraceptive($family_planning_status, $start_date, $end_date, $period);
        //set up csv file
        $file_name = "contraceptive.csv";
        $file_content = "";

        $this->load->helper("download");

        $column_names = array(
            $updated_data['x_axis_label'],
            "Current/Continuing User (CU)",
            "Changing Clinic (CC)",
            "Restart (RS)",
            "Changing Method (CM)",
            "Learning User (LU)",
            "New Acceptor (NA)"
            );

        $col_count = count($column_names);
        $row_count = count($updated_data['cu_count']);

        foreach($column_names as $index => $column){
            $file_content .= $column;
            if($index + 1 < $col_count){
                $file_content .= ",";
            }else{
                $file_content .= "\r\n";
            }
        }

        for($i = 0; $i < $row_count; $i++){
            $file_content.= $updated_data['labels'][$i].","
                            .$updated_data['cu_count'][$i].","
                            .$updated_data['cc_count'][$i].","
                            .$updated_data['rs_count'][$i].","
                            .$updated_data['cm_count'][$i].","
                            .$updated_data['lu_count'][$i].","
                            .$updated_data['na_count'][$i];
            $file_content .= "\r\n";
        }

        force_download($file_name, $file_content);

    }

    private function add_missing_labels_contraceptive($warehouse, $start_date, $end_date, $period){

        $data['labels'] = array();
        $data['cu_count'] = array();
        $data['cc_count'] = array();
        $data['rs_count'] = array();
        $data['cm_count'] = array();
        $data['lu_count'] = array();
        $data['na_count'] = array();

        if(strcasecmp($period, "yearly") === 0){
            $data['labels'] = range(idate("Y", strtotime($start_date)), idate("Y", strtotime($end_date)));
            $data['x_axis_label'] = "Year";
            $row = 0;
            foreach($data['labels'] as $curr_year){
                if ($row < sizeof($warehouse)) {
                    if($warehouse[$row]['year'] == $curr_year){
                        array_push($data['cu_count'],$warehouse[$row]['cu_count']);
                        array_push($data['cc_count'],$warehouse[$row]['cc_count']);
                        array_push($data['rs_count'],$warehouse[$row]['rs_count']);
                        array_push($data['cm_count'],$warehouse[$row]['cm_count']);
                        array_push($data['lu_count'],$warehouse[$row]['lu_count']);
                        array_push($data['na_count'],$warehouse[$row]['na_count']);
                        $row++;
                    }else{
                        array_push($data['cu_count'],0);
                        array_push($data['cc_count'],0);
                        array_push($data['rs_count'],0);
                        array_push($data['cm_count'],0);
                        array_push($data['lu_count'],0);
                        array_push($data['na_count'],0);
                    }
                }
            }
        }elseif (strcasecmp($period, "monthly") === 0) {
            $data['labels'] = $this->dateRange($start_date, $end_date, '+1 month', "M 'y");
            $data['x_axis_label'] = "Month";
            $row = 0;
            foreach($data['labels'] as $curr_month_year){
                if ($row < sizeof($warehouse)) {
                    $curr_year_str = substr($curr_month_year, -2);
                    $curr_month_str = substr($curr_month_year, 0, 3);
                    $curr_year = '20'.$curr_year_str;
                    if($curr_year > date('Y')) {
                        $curr_year = $curr_year - 100;
                    }
                    $curr_month = idate("m", strtotime($curr_month_str));
                    if($warehouse[$row]['year'] == $curr_year && $warehouse[$row]['month'] == $curr_month){
                        array_push($data['cu_count'],$warehouse[$row]['cu_count']);
                        array_push($data['cc_count'],$warehouse[$row]['cc_count']);
                        array_push($data['rs_count'],$warehouse[$row]['rs_count']);
                        array_push($data['cm_count'],$warehouse[$row]['cm_count']);
                        array_push($data['lu_count'],$warehouse[$row]['lu_count']);
                        array_push($data['na_count'],$warehouse[$row]['na_count']);
                        $row++;
                    }else{
                        array_push($data['cu_count'],0);
                        array_push($data['cc_count'],0);
                        array_push($data['rs_count'],0);
                        array_push($data['cm_count'],0);
                        array_push($data['lu_count'],0);
                        array_push($data['na_count'],0);
                    }
                }
            }
        }elseif (strcasecmp($period, "quarterly") === 0) {
            $data['labels'] = $this->dateRange($start_date, $end_date, '+1 month', "M 'y");
            $data['x_axis_label'] = "Quarter";
            foreach($data['labels'] as &$month_year){
                $month = substr($month_year, 0, 3);
                if(strcasecmp($month, "jan") === 0 || strcasecmp($month, "feb") === 0 || strcasecmp($month, "mar") === 0){
                    $month_year = str_replace($month, "Q1", $month_year);
                }elseif (strcasecmp($month, "apr") === 0 || strcasecmp($month, "may") === 0 || strcasecmp($month, "jun") === 0) {
                    $month_year = str_replace($month, "Q2", $month_year);
                }elseif (strcasecmp($month, "jul") === 0 || strcasecmp($month, "aug") === 0 || strcasecmp($month, "sep") === 0) {
                    $month_year = str_replace($month, "Q3", $month_year);
                }elseif (strcasecmp($month, "oct") === 0 || strcasecmp($month, "nov") === 0 || strcasecmp($month, "dec") === 0) {
                    $month_year = str_replace($month, "Q4", $month_year);
                }
            }
            $data['labels'] = array_unique($data['labels']);
            $data['labels'] = array_values($data['labels']); //compresses the array

            $row = 0;
            foreach($data['labels'] as $curr_quarter_year){
                if ($row < sizeof($warehouse)) {
                    $curr_year_str = substr($curr_quarter_year, -2);
                    $curr_quarter_str = substr($curr_quarter_year, 1, 1);
                    $curr_year = '20'.$curr_year_str;
                    if($curr_year > date('Y')) {
                        $curr_year = $curr_year - 100;
                    }
                    $curr_quarter = intval($curr_quarter_str);
                    if($warehouse[$row]['year'] == $curr_year && $warehouse[$row]['quarter'] == $curr_quarter){
                        array_push($data['cu_count'],$warehouse[$row]['cu_count']);
                        array_push($data['cc_count'],$warehouse[$row]['cc_count']);
                        array_push($data['rs_count'],$warehouse[$row]['rs_count']);
                        array_push($data['cm_count'],$warehouse[$row]['cm_count']);
                        array_push($data['lu_count'],$warehouse[$row]['lu_count']);
                        array_push($data['na_count'],$warehouse[$row]['na_count']);
                        $row++;
                    }else{
                        array_push($data['cu_count'],0);
                        array_push($data['cc_count'],0);
                        array_push($data['rs_count'],0);
                        array_push($data['cm_count'],0);
                        array_push($data['lu_count'],0);
                        array_push($data['na_count'],0);
                    }
                }
            }
        }

        return $data;
    }
    










    /**
     * NUMBER OF PHILHEALTH BENEFICIARIES: DISAGGRATED BY STATUS CONTROLLER FUNCTIONS ==================================
     *
     * @author Lorenz Timothy Barco Ranera <lbranera@up.edu.ph>
     */

    private function get_philhealth_by_status_chart_info($warehouse){
        $labels = array();
        $expired = array();
        $active = array();
        $expiredDP = array();
        foreach($warehouse as $i){
          $tmp = 0;
          foreach($i as $j){
            if($tmp == 0){
              array_push($labels, $j);
            }else if($tmp == 1){
               array_push($active, $j);
            }else if($tmp == 2){
               array_push($expired, $j);
            }else if($tmp == 3){
               array_push($expiredDP, $j);
            }
            $tmp++;
          }
        }

        $chart_info = array("labels" => $labels,
                            "datasets" => array(
                            array("label" => "Active" , "backgroundColor" => "#039be5", "data" => $active),
                            array("label" => "Expired"  , "backgroundColor" => "#ffa726", "data" => $expired),
                            array("label" => "Expired During Period", "backgroundColor" => "#66bb6a", "data" => $expiredDP)
                            )
        );


        return $chart_info;
    }

    private function get_philhealth_by_status_table_info($warehouse,$period){
        $table = array();
        $columns = array();
        
        if(strcasecmp($period, "quarterly") === 0){
          $period_column = "Quarter";
        }else if(strcasecmp($period, "yearly") === 0){
          $period_column = "Year";
        }else if(strcasecmp($period, "monthly") === 0){
          $period_column = "Month";
        }

        $column_names = array($period_column,"Active","Expired","Expired During Period");
        foreach($warehouse as $i){
          $row = array();
          foreach($i as $j){
            array_push($row, $j);
          }
          array_push($table,$row);
        }

        for($i =0; $i<4; $i++){
           $columns[$i] = array("title" => $column_names[$i]);
        }

        $table_info = array("columns" => $columns,
                            "data" => $table
                      );
        return $table_info;
    }

    public function philhealth_by_status($area="kvhc", $period="yearly", $start_date='1970-01-01', $end_date=null){
        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $this->load->model("data_model");
        $final_warehouse = $this->data_model->get_philhealth_by_status($area, $start_date, $end_date, $period);
        $data["warehouse"] = $final_warehouse;
        $data["chart_info"] = $this->get_philhealth_by_status_chart_info($data["warehouse"]);
        if(strcasecmp($period, "quarterly") === 0){
          $data['x_axis'] = "Quarter";
        }else if(strcasecmp($period, "yearly") === 0){
          $data['x_axis'] = "Year";
        }else if(strcasecmp($period, "monthly") === 0){
          $data['x_axis'] = "Month";
        }

        if(strcasecmp($area,'kvhc')===0){
            $the_area = "KVHC";
        }else if(strcasecmp($area,'phase2area1')===0){
            $the_area = "Phase 2 Area 1";
        }else{
            $the_area = ucfirst($area);
        }
        $data['y_axis'] = "Number of PhilHealth Beneficiaries";
        $data['title'] =  ucfirst($period).' Number Of PhilHealth Beneficiaries: Disaggregated by Status in '.$the_area;
        $this->load->view('philhealth_by_status', $data);
    }
    
     public function philhealth_by_status_table($area="kvhc", $period="yearly", $start_date='1970-01-01', $end_date=null){
        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $this->load->model("data_model");
        $final_warehouse = $this->data_model->get_philhealth_by_status($area, $start_date, $end_date, $period);
        $data["warehouse"] = $final_warehouse;
        $data["table_info"] = $this->get_philhealth_by_status_table_info($data["warehouse"],$period);
        $this->load->view('philhealth_by_status_table', $data);
    }
    
    public function philhealth_by_status_table_csv($area = "phase2area1", $period = "yearly", $start_date = '1970-01-01', $end_date = null){
        $area = strtolower($area);
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos", "philippines");

        if(!in_array($area, $areas)){
            show_404();
        }

        if(is_null($end_date)){
            $end_date = date("Y-m-d", time());
        }

        $period = strtolower($period);
        $periods = array("yearly", "quarterly", "monthly");

        if(!in_array($period, $periods)){
            show_404();
        }

        $start_date .= ' 00:00:00';
        $end_date .= ' 00:00:00';

        $this->load->model("data_model");
        $warehouse = $this->data_model->get_philhealth_by_status($area, $start_date, $end_date, $period);
        
        $table_info = $this->get_philhealth_by_status_table_info($warehouse, $period);

        $file_name = strtolower($period) . "-number-of-philhealth-beneficiaris-disaggregated-by-status-in-" . strtolower($area) .
                              "-from-" . date("Y-m-d", strtotime($start_date)) .
                              "-to-" . date("Y-m-d", strtotime($end_date)) . ".csv";

        $this->load->helper("download");

        $file_content = "";
        $col_count = count($table_info["columns"]);
        $row_count = count($table_info["data"]);

        foreach($table_info["columns"] as $index => $column){
            $file_content .= $column["title"];
            if($index + 1 < $col_count){
                $file_content .= ",";
            }else{
                $file_content .= "\r\n";
            }
        }

        foreach($table_info["data"] as $row_index => $row){
            foreach($row as $col_index => $cell){
                $file_content .= $cell;
                if($col_index + 1 < $col_count){
                    $file_content .= ",";
                }else if($row_index + 1 < $row_count){
                    $file_content .= "\r\n";
                }
            }
        }

        force_download($file_name, $file_content);        
    }

    /** END OF NUMBER OF PHILHEALTH BENEFICIARIES: DISAGGREGATED BY STATUS CONTROLLER FUNCTIONS ================== */
    











    /**
     * FULLY IMMUNIZED CHILDREN CONTROLLER FUNCTIONS ================================================================
     *  
     *   @author Harold R. Mansilla <hrmansilla08@gmail.com>
     */
   
    public function fully_immunized($Area="kvhc", $Period="monthly", $Start="2012-01-01", $End="2013-01-01"){
        $data = array('Start' => $Start, 'End' => $End, 'Period', 'Period' => $Period, 'Area' => $Area);
        $this->load->view('fully_immunized',$data);
    }
    
    /**
     * [get_fully_immunized description]
     * @return [type] [description]
     */
    public function get_fully_immunized(){
        $this->load->model("data_model");
        if(isset($_REQUEST)){
            $Start = htmlspecialchars($this->input->get('Start'));
            $End = htmlspecialchars($this->input->get('End'));
            $Period = htmlspecialchars($this->input->get('Period'));
            $Area = htmlspecialchars($this->input->get('Area'));
            $this->data_model->set_immunizations_view($Area);
            $res = $this->data_model->get_fully_immnunized_children($Start, $End, $Period, $Area);
            echo json_encode($res);
        }
    }

    /** END OF FULLY IMMUNIZED CHILDREN MODEL FUNCTIONS ============================================================ */









   /**
    *  DEWORMING CONTROLLER FUNCTIONS =============================================================================
    *  
    *  @author Virgilio M. Mendoza III <vmmendoza1@up.edu.ph>
    */

   public function deworming($Area="kvhc", $Period="monthly", $Start="2012-01-01", $End="2013-01-01"){
        $data = array('Start' => $Start, 'End' => $End, 'Period' => $Period, 'Area' => $Area);
        $this->load->view('deworming',$data);

    }

    public function get_deworming(){
        $this->load->model("data_model");
        if(isset($_REQUEST)){
            $Start = htmlspecialchars($this->input->get('Start'));
            $End = htmlspecialchars($this->input->get('End'));
            $Period = htmlspecialchars($this->input->get('Period'));
            $Area = htmlspecialchars($this->input->get('Area'));
            $this->data_model->set_view($Area);
            $res = $this->data_model->get_deworming2($Start, $End, $Period, $Area);
            echo json_encode($res);
        }
    }
    /** END OF DEWROMING CONTROLLER FUNCTIONS ================================================================= */






    /**
     * NUMBER OF PHILHEALTH BENEFICIARIES: DISAGGREGATED BY ROLE CONTROLLER FUNCTIONS =============================
     *
     *  @author John Robinson S. Francisco <jsfrancisco5@up.edu.ph>
     *  @author Sean Louie Pineda <pinedaseanlouie@gmai>
     */
    
    public function philhealth_archaic($Area="kvhc",$Period="monthly",$Start="2012-01-01", $End="2013-01-01"){
        // echo $Area;
        $data = array('Start' => $Start, 'End' => $End, 'Period', 'Period' => $Period, 'Area' => $Area);
        $this->load->view('philhealth_archaic',$data);
    }

    public function get_philhealth_archaic(){
        $this->load->model("data_model");
        if(isset($_REQUEST)){
            $Start = htmlspecialchars($this->input->get('Start'));
            $End = htmlspecialchars($this->input->get('End'));
            $Period = htmlspecialchars($this->input->get('Period'));
            $Area = htmlspecialchars($this->input->get('Area'));
            $this->data_model->set_philhealth_role_view($Area);
            $res = $this->data_model->get_philhealth_archaic_role($Area,$Period,$Start,$End) ;
            echo json_encode($res);
        }
    }
    
    /** END OF NUMBER OF PHILHEALTH BENEFICIARIES: DISAGGREGATED BY ROLE CONTROLLER FUNCTIONS ============ */








    /**
     *  EXCLUSIVE BREASTFEEDING CONTROLLER FUNCTIONS =================================================================
     *     
     *  @author Reena Myka De Guzman <[<email address>]>
     */
    
    public function exclusive_breastfeeding($Area="kvhc", $Period="monthly", $Start="2012-01-01", $End="2013-01-01"){
        $data = array('Start' => $Start, 'End' => $End, 'Period' => $Period, 'Area' => $Area);
        $this->load->view('exclusive_breastfeeding',$data);
    }
    
    public function get_exclusive_breastfeeding(){
        $this->load->model("data_model");
        if(isset($_REQUEST)){
            $Start = htmlspecialchars($this->input->get('Start'));
            $End = htmlspecialchars($this->input->get('End'));
            $Period = htmlspecialchars($this->input->get('Period'));
            $Area = htmlspecialchars($this->input->get('Area'));
            $this->data_model->set_exclusive_breastfeeding_view($Area);
            $this->data_model->set_exclusive_breastfeeding_view_selected($Area);
            $res = $this->data_model->get_exclusivebreastfeeding2($Start, $End, $Period, $Area);
            echo json_encode($res);
        }
    }

    /** END OF EXCLUSIVE BREASTFEEDING CONTROLLER FUNCTIONS ========================================================= */




    
    private function dateRange($first, $last, $step = '+1 day', $format = 'Y/m/d') {
    	$dates = array();
    	$current = strtotime( $first );
    	$last = strtotime( $last );

    	while( $current <= $last ) {

    		$dates[] = date( $format, $current );
    		$current = strtotime( $step, $current );
    	}
    	return $dates;
    }

}
