<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo base_url('assets/css/materialize.css')?>" type="text/css" rel="stylesheet" media="screen,projection"/>
	<style type="text/css">
	#mycanvas{
		border-color: red;
	}
	.chart-container {
		position: relative;
		margin: auto;
		    height: 500px;
    width: 500px;
	}
</style>
</head>
<body bgcolor="#FFFFFF">

<!-- <div id="chart-container"> -->
	<canvas id="mycanvas" width="400" height="400"></canvas>
<!-- </div> -->

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<script>
	var dat;
	$(document).ready(function(){
		$.ajax({
			url: "<?=site_url()?>/data/get_exclusive_breastfeeding",
			method: "GET",
			data: ({Start: '<?=$Start?>', End: '<?=$End?>', Period: '<?=$Period?>', Area: '<?=$Area?>'}),
			dataType: 'json',
			beforeSend: function(){
				$('#loader').show();
			},
			complete: function(){
				$('#loader').hide();
			},
			success: function(data) {
				dat = data;
				console.log("AJAX SUCCESS");
				console.log(data);
				var month = [];
				var value = [];

				data.forEach(function(obj) {
					month.push(obj.Period);
					value.push(obj.Number);
				});

				var chartdata = {
					labels: month,
					datasets : [
					{
						label: 'Number of Exclusively Breastfed Children',
						backgroundColor: 'rgba(255, 99, 132, 1)',
						borderColor: 'rgba(255, 99, 132, 1)',
						hoverBackgroundColor: 'rgba(255, 125, 132, 1)',
						hoverBorderColor: 'rgba(255, 125, 132, 1)',
						data: value
					}
					]
				};

				var ctx = $("#mycanvas");

				var barGraph = new Chart(ctx, {
					type: 'bar',
					data: chartdata,
					options: {

						title: {
							display: true,
							text: 'Exclusive Breastfeeding (Number of Children Breastfed)',
							fontSize: 16,
							fontFamily: "Roboto",
						},
						scales:{
							xAxes:[{
								scaleLabel:{
									display:true,
									labelString: 'Dates'
								}
							}],
							yAxes:[{
								ticks:{
									beginAtZero:true
								},
								scaleLabel:{
									display:true,
									labelString: 'Exclusive Breastfeeding Data'
								}
							}]
						}
					}

				});
			},
			error: function(data) {
				console.log("AJAX ERROR");
				console.log(data);
			}
		});
	});
</script>
</body>
</html>