<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo base_url('assets/css/materialize.css')?>" type="text/css" rel="stylesheet" media="screen,projection"/>
	<style type="text/css">
	.chart-container {
		position: relative;
		margin: auto;
		    height: 500px;
    width: 500px;
	}
</style>
</head>
<body bgcolor="#FFFFFF">
	<!-- <div id="loader" class="loader"> -->
		
		<!-- <div id="chart-container"> -->
		<canvas id="mycanvas" width="400" height="400"></canvas>
		<!-- </div> -->


		<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="<?php echo base_url('assets/js/materialize.js')?>"></script>
		<script src="<?php echo base_url('assets/js/init.js')?>"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
		<script>
			var dat;
			$(document).ready(function(){
				$.ajax({
					url: "<?=site_url()?>/data/get_fully_immunized",
					method: "GET",
					data: ({Start: '<?=$Start?>', End: '<?=$End?>', Period: '<?=$Period?>', Area: '<?=$Area?>'}),
					dataType: 'json',
					beforeSend: function(){
						$('#loader').show();
					},
					complete: function(){
						$('#loader').hide();
					},
					success: function(data) {
						dat = data;
						console.log("AJAX SUCCESS");
						console.log(data);
						var count = [];
						var period = []
						data.forEach(function(obj) {
							count.push(obj.Number);
							period.push(obj.Period);
						});

						var chartdata = {
							labels: period,
							datasets : [
							{
								label: 'Count',
								backgroundColor: 'rgba(99, 255, 132, 1)',
								borderColor: 'rgba(99, 255, 132, 1)',
								hoverBackgroundColor: 'rgba(125, 255, 132, 1)',
								hoverBorderColor: 'rgba(125, 255, 132, 1)',
								data: count
							}
							]
						};

						var ctx = $("#mycanvas");

						var barGraph = new Chart(ctx, {
							type: 'bar',
							data: chartdata,
							options: {
								title: {
									display: true,
									text: 'Fully-Immunized Children',
									fontSize: 16,
									fontFamily: "Roboto",
								},
								scales: {
									xAxes: [{
										scaleLabel:{
											display: true,
											labelString: "Period"
										}
									}],
									yAxes: [{
										ticks: {
											beginAtZero: true
										},
										scaleLabel:{
											display: true,
											labelString: "No. of Children"
										}
									}]
								}
							}

						});
					},
					error: function(data) {
						console.log("AJAX ERROR");
						console.log(data);
					}
				});
			});
		</script>

	</body>
	</html>