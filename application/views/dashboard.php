<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0" />
  <title>CHITS</title>
  <link rel="shortcut icon" href="<?=base_url('assets/images/photo.jpg')?>">
  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/materialize.css')?>" type="text/css" rel="stylesheet" media="screen,projection"
  />
  <link href="<?php echo base_url('assets/css/style.css')?>" type="text/css" rel="stylesheet" media="screen,projection" />

  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection" />

  
  <style>
  iframe {
    background-image: url("assets/images/spinner.gif");   
    background-repeat: no-repeat;
    background-position: center; 
  }
</style>
</head>

<body>
    
  <nav class="light-blue lighten-1" role="navigation">
    <div class="nav-wrapper container">
      <a id="logo-container" href="#" class="brand-logo">rCHITS</a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#prequels">Prequels</a></li>
        <li><a href="#filters">Indicators</a></li>
      </ul>
      
      <ul id="nav-mobile" class="sidenav">

      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger">
        <i class="material-icons">menu</i>
      </a>
    </div>
  </nav>

  <main>
    <div id="table_modal" class="modal">
      <div class="modal-content" id="modal_body">

      </div>
      <div class="modal-footer">
        <a href="#!" class="btn-flat">Save as CSV</a>
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
      </div>
    </div>
    <div class>
    <h3 id="prequels"> Prequels </h3>
      <div class="section center-align">
        <div class="row">
          <div class="col m6 s12">
            <iframe id="disaggregated_births" src="http://localhost/chits/index.php/data/disaggregated_births" height="500" width="500" frameborder="0"></iframe>
          </div>
              <!-- <div class="col m6 s12">
                <iframe id="fully_immunized" src="<?=site_url();?>/data/fully_immunized/" height="500" width="500" frameborder="0"></iframe>
              </div> -->
              <div class="col m6 s12">
                <iframe id="population_by_age_bracket" src="http://localhost/chits/index.php/data/population_by_age_bracket/" height="500" width="500" frameborder="0"></iframe>
              </div>
            </div> 
            <div class="row">
              <div class="col m6 s12">
                <iframe id="consultation" src="http://localhost/chits/index.php/data/consultation" height="500px"
                width="500px" frameborder="0"></iframe>
              </div>
            </div>
          </div>
          <hr>
          <div class="section ">
            <div class="row">
              <div class="col s12 m12">
                <form id="filters">
                  <h3>Filters</h3>
                  <div class="input-field col s2">
                    <select id="country">
                      <option value="" disabled>Philippines</option>
                    </select>
                    <label>Area</label>
                  </div>

                  <div class="input-field col s4">
                    <select id="region" disabled>
                      <option value="" disabled>Select region</option>
                      <option value="01">REGION I (ILOCOS REGION)</option>
                      <option value="02">REGION II (CAGAYAN VALLEY)</option>
                      <option value="03">REGION III (CENTRAL LUZON)</option>
                      <option value="04">REGION IV-A (CALABARZON)</option>
                      <option value="17">REGION IV-B (MIMAROPA)</option>
                      <option value="05">REGION V (BICOL REGION)</option>
                      <option value="06">REGION VI (WESTERN VISAYAS)</option>
                      <option value="07">REGION VII (CENTRAL VISAYAS)</option>
                      <option value="08">REGION VIII (EASTERN VISAYAS)</option>
                      <option value="09">REGION IX (ZAMBOANGA PENINSULA)</option>
                      <option value="10">REGION X (NORTHERN MINDANAO)</option>
                      <option value="11">REGION XI (DAVAO REGION)</option>
                      <option value="12">REGION XII (SOCCSKSARGEN)</option>
                      <option value="13" selected>NATIONAL CAPITAL REGION (NCR)</option>
                      <option value="14">CORDILLERA ADMINISTRATIVE REGION (CAR)</option>
                      <option value="15">AUTONOMOUS REGION IN MUSLIM MINDANAO (ARMM)</option>
                      <option value="16">REGION XIII (Caraga)</option>
                    </select>
                    <label>Region</label>
                  </div>

                  <div class="input-field col s2">
                    <select id="province13" disabled>
                      <option value="1339">NCR, CITY OF MANILA</option>
                      <option value="1374">NCR, SECOND DISTRICT</option>
                      <option value="1375" selected>NCR, THIRD DISTRICT</option>
                      <option value="1376">NCR, FOURTH DISTRICT</option>
                    </select>
                    <label>Province</label>
                  </div>

                  <div class="input-field col s2">
                    <select id="city1375" disabled>
                      <option value="137503">CITY OF NAVOTAS</option>
                    </select>
                    <label>City/Municipality</label>
                  </div>

                  <div class="input-field col s2">
                    <select id="area">
                      <option value="" disabled>Choose your option</option>
                      <option value="philippines">All</option>
                      <option value="kvhc" selected>KVHC</option>
                      <option value="phase2area1">Phase 2 Area 1</option>
                      <option value="sipacalmacen">Sipacalmacen</option>
                      <option value="tangos">Tangos</option>
                    </select>
                    <label>Health Center</label>
                  </div>
                  <div class="input-field col s2">
                    <select id="period" onchange="changePeriod()">
                      <option value="" disabled>Choose your option</option>
                      <option value="yearly">Annually</option>
                      <option value="quarterly">Quarterly</option>
                      <option value="monthly" selected>Monthly</option>
                      <option value="custom">Custom</option>
                    </select>
                    <label>Period</label>
                  </div>
                  <div class="input-field col s2" id="sQuarterDiv">
                    <select id="reporting_period_s_quarter">
                      <option value="" disabled>Month</option>
                      <option value="1" selected>January</option>
                      <option value="4">April</option>
                      <option value="7">July</option>
                      <option value="10">October</option>
                    </select>
                    <label>Quarter</label>
                  </div>
                  <div class="input-field col s2" id="sMonthDiv">
                    <select id="reporting_period_s_month">
                      <option value="" disabled>Month</option>
                      <option value="1" selected>January</option>
                      <option value="2">February</option>
                      <option value="3">March</option>
                      <option value="4">April</option>
                      <option value="5">May</option>
                      <option value="6">June</option>
                      <option value="7">July</option>
                      <option value="8">August</option>
                      <option value="9">September</option>
                      <option value="10">October</option>
                      <option value="11">November</option>
                      <option value="12">December</option>
                    </select>
                    <label>Start Month</label>
                  </div>
                  <div class="input-field col s2" id="sYearDiv">
                    <select id="reporting_period_s_year">
                      <?php for($i = 1970; $i <= 2018; $i++):?>
                        <option value="<?=$i?>" <?=( $i==2012 ? "selected" : "")?>><?=$i?></option>
                      <?php endfor;?>
                    </select>
                    <label>Start Year</label>
                  </div>
                  <div class="input-field col s2" id="eQuarterDiv">
                    <select id="reporting_period_e_quarter">
                      <?php for($i = 1970; $i <= 2018; $i++):?>
                        <option value="<?=$i?>" <?=( $i==2012 ? "selected" : "")?>><?=$i?></option>
                      <?php endfor;?>
                    </select>
                    <label>Year</label>
                  </div>
                  <div class="input-field col s2" id="eMonthDiv">
                    <select id="reporting_period_e_month">
                      <option value="" disabled>Month</option>
                      <option value="1" selected>January</option>
                      <option value="2">February</option>
                      <option value="3">March</option>
                      <option value="4">April</option>
                      <option value="5">May</option>
                      <option value="6">June</option>
                      <option value="7">July</option>
                      <option value="8">August</option>
                      <option value="9">September</option>
                      <option value="10">October</option>
                      <option value="11">November</option>
                      <option value="12">December</option>
                    </select>
                    <label>End Month</label>
                  </div>
                  <div class="input-field col s2" id="eYearDiv">
                    <select id="reporting_period_e_year">
                      <option value="" disabled>Year</option>
                      <option value="2005">2005</option>
                      <option value="2006">2006</option>
                      <option value="2007">2007</option>
                      <option value="2008">2008</option>
                      <option value="2009">2009</option>
                      <option value="2010">2010</option>
                      <option value="2011">2011</option>
                      <option value="2012">2012</option>
                      <option value="2013" selected>2013</option>
                      <option value="2014">2014</option>
                      <option value="2015">2015</option>
                      <option value="2016">2016</option>
                      <option value="2017">2017</option>
                      <option value="2018">2018</option>
                    </select>
                    <label>End Year</label>
                  </div>
                  <div class="input-field col s2" id="sCustomDiv">
                    <input type="date" class="datepicker" id="reporting_period_s_custom">
                    <label>Start Date</label>
                  </div>
                  <div class="input-field col s2" id="eCustomDiv">
                    <input type="date" class="datepicker" id="reporting_period_e_custom">
                    <label>End Date</label>
                  </div>
                  <br>
                  <div class="input-field col s12 center-align">
                    <button id="update_button" type="button" class="btn waves-effect waves-light">Update
                      <i class="material-icons right">update</i>
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>



          <!-- Modal Structure -->
          <div id="birth_rate_modal" class="modal modal-fixed-footer">
            <div class="modal-content">
              <h4>Crude Birth Rate</h4>
              <table id="birth_rate_table">
                <thead>
                  <tr>
                    <th>Period</th>
                    <th>Birth Rate (in percentage)</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <div class="modal-footer">
              <a id="birth_rate_csv" href="http://localhost/chits/index.php/data/birth_rate_csv/kvhc/monthly/2012-01-01/2013-01-01" class="modal-close waves-effect waves-green btn-flat">Save as CSV</a>
              <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
          </div>

          <!-- Modal Structure -->
          <div id="births_modal" class="modal modal-fixed-footer">
            <div class="modal-content">
              <h4>Births Attended by Skilled Professional</h4>
              <table id="births_table">
                <thead>
                  <tr>
                    <th>Period</th>
                    <th>Doctor</th>
                    <th>Midwife</th>
                    <th>Nurse</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <div class="modal-footer">
              <a id="births_csv" href="http://localhost/chits/index.php/data/births_table_csv/kvhc/monthly/2012-01-01/2013-01-01" class="modal-close waves-effect waves-green btn-flat">Save as CSV</a>
              <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
          </div>

          <!-- Modal Structure -->
          <div id="contraceptive_modal" class="modal modal-fixed-footer">
            <div class="modal-content">
              <h4>Contraceptive and family planning users: Status</h4>
              <table id="contraceptive_table">
                <thead>
                  <tr>
                    <th>Period</th>
                    <th>Number of CU</th>
                    <th>Number of CC</th>
                    <th>Number of RS</th>
                    <th>Number of CM</th>
                    <th>Number of LU</th>
                    <th>Number of NA</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <div class="modal-footer">
              <a id="contraceptive_csv" href="http://localhost/chits/index.php/data/contraceptive_csv/kvhc/monthly/2012-01-01/2013-01-01" class="modal-close waves-effect waves-green btn-flat">Save as CSV</a>
              <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
          </div>

          <!-- Modal Structure -->
          <div id="philhealth_by_status_modal" class="modal modal-fixed-footer">
            <div class="modal-content">
              <h4>Number of philhealth beneficiaries: Disaggregated by status</h4>
              <table id="philhealth_by_status_table">
                <thead>
                  <tr>
                    <th>Period</th>
                    <th>Number of Active</th>
                    <th>Number of Expired</th>
                    <th>Number of Expired during Period</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </div>
            <div class="modal-footer">
              <a id="philhealth_by_status_csv" href="http://localhost/chits/index.php/data/philhealth_by_status_table_csv/kvhc/monthly/2012-01-01/2013-01-01" class="modal-close waves-effect waves-green btn-flat">Save as CSV</a>
              <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
          </div>



          <div class="section center-align">

            <div class="row">
              <div class="col m6 s12">
                <iframe id="birthrate" class="iuframe" src="http://localhost/chits/index.php/data/birthrate/kvhc/monthly/2012-01-01/2013-01-01" height="500px"
                width="500px" frameborder="0"></iframe><br>
                <!-- Modal Trigger -->
                
              </div>
              <div class="col m6 s12">
                <iframe id="births" class="iuframe" src="http://localhost/chits/index.php/data/births/kvhc/monthly/2012-01-01/2013-01-01" height="500" width="500"
                frameborder="0"></iframe><br>
                <!-- Modal Trigger -->
                
              </div>
            </div>

            <div class="row">
              <div class="col m6 s12">

                <iframe id="contraceptive" class="iuframe" src="http://localhost/chits/index.php/data/contraceptive/kvhc/monthly/2012-01-01/2013-01-01" height="500px"
                width="500px" frameborder="0"></iframe><br>
                <!-- Modal Trigger -->
              </div>
              <div class="col m6 s12">
                <iframe id="philhealth_by_status" class="iuframe" src="http://localhost/chits/index.php/data/philhealth_by_status/kvhc/monthly/2012-01-01/2013-01-01"
                height="500px" width="500px" frameborder="0"></iframe><br>
                <!-- Modal Trigger -->  
              </div>
            </div>

            <div class="row">
              <div class="col m6 s12">
                <iframe id="deworming" class="ourframe" src="http://localhost/chits/index.php/data/deworming/" height="500" width="500" frameborder="0"></iframe>
              </div>
              <div class="col m6 s12">
                <iframe id="fully_immunized" class="ourframe" src="http://localhost/chits/index.php/data/fully_immunized/" height="500" width="500" frameborder="0"></iframe>
              </div>
            </div>
            <div class="row">
              <div class="col m6 s12">
                <iframe id="philhealth_archaic" class="ourframe" src="http://localhost/chits/index.php/data/philhealth_archaic/" height="500" width="500"
                frameborder="0"></iframe>
              </div>
              <div class="col m6 s12">
                <iframe id="exclusive_breastfeeding" class="ourframe" src="http://localhost/chits/index.php/data/exclusive_breastfeeding/" height="500" width="500" frameborder="0"></iframe>
              </div>
            </div>

          </div>
          <div class="section">
            <div id="params">

            </div>
          </div>
        </div>
      </main>
      <footer class="page-footer orange">
        <div class="footer-copyright">
          <div class="container">
            Made by
            <a class="orange-text text-lighten-3" href="">NOPE</a>
          </div>
        </div>
      </footer>


      <!--  Scripts-->
      <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script src="<?php echo base_url('assets/js/materialize.js')?>"></script>
      <script src="<?php echo base_url('assets/js/init.js')?>"></script>

      <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
      <script type="text/javascript">
        $(document).ready(function () {
          $(".dropdown-button").dropdown();

          $("#birth_rate_table").dataTable({
            ajax: "http://localhost/chits/index.php/data/birth_rate_table/kvhc/monthly/2012-01-01/2013-01-01",
            "aaSorting": []
          })
          $("#births_table").dataTable({
            ajax: "http://localhost/chits/index.php/births_table/kvhc/monthly/2012-01-01/2013-01-01",
            "aaSorting": []
          })
          $("#contraceptive_table").dataTable({
            ajax: "http://localhost/chits/index.php/data/contraceptive_table/kvhc/monthly/2012-01-01/2013-01-01",
            "aaSorting": []
          })
          $("#philhealth_by_status_table").dataTable({
            ajax: "http://localhost/chits/index.php/data/philhealth_by_status_table/kvhc/monthly/2012-01-01/2013-01-01",
            "aaSorting": []
          })
          $('select').formSelect();
          $('.modal').modal();
          changePeriod();
          $('#table_modal').modal({
            onCloseEnd: function(){
              $('#content_table').remove();
              $('#modal_body').empty();
            }
          });
        });


        function changePeriod() {
          var e = document.getElementById("period");
          var strUser = e.options[e.selectedIndex].value;
          document.getElementById("sMonthDiv").style.display = "none";
          document.getElementById("eMonthDiv").style.display = "none";
          document.getElementById("sYearDiv").style.display = "none";
          document.getElementById("eYearDiv").style.display = "none";
          document.getElementById("sQuarterDiv").style.display = "none";
          document.getElementById("eQuarterDiv").style.display = "none";
          document.getElementById("sCustomDiv").style.display = "none";
          document.getElementById("eCustomDiv").style.display = "none";
          if (strUser == "yearly") {
            document.getElementById("sYearDiv").style.display = "block";
            document.getElementById("eYearDiv").style.display = "block";
          }
          if (strUser == "monthly") {
            document.getElementById("sMonthDiv").style.display = "block";
            document.getElementById("eMonthDiv").style.display = "block";
            document.getElementById("sYearDiv").style.display = "block";
            document.getElementById("eYearDiv").style.display = "block";

          }
          if (strUser == "quarterly") {
            document.getElementById("sQuarterDiv").style.display = "block";
            document.getElementById("eQuarterDiv").style.display = "block";
          }
          if (strUser == "custom") {
            document.getElementById("sCustomDiv").style.display = "block";
            document.getElementById("eCustomDiv").style.display = "block";

          }
        }   
      </script>
   <!--RELIANT 'TO SA IFRAME NA TAG SO PAPALIT NA LANG
    DAPAT DIN LAHAT NG MGA CHARTS MAY 'dat' NA VARIABLE NA LAMAN ANG DATA-->
    <script type="text/javascript">
      $('.ourframe').load(function(){
        var iframe = $(this).contents();
        var test = $(this, top.document);
        iframe.find("#mycanvas").click(function(){
          var title;
          if(test.attr('id') == 'deworming'){
            title = "Deworming Coverage";
          }
          else if (test.attr('id') == "disaggregated_births"){
            title = "Births Disaggregated by Method";
          }
          else if (test.attr('id') == 'fully_immunized') {
            title = "Fully Immunized Children";
          }
          else if (test.attr('id') == 'philhealth_archaic'){
            title = "Number of PhilHealth Beneficiaries (Disaggregated by Roles)";
          }
          else if (test.attr('id') == 'exclusive_breastfeeding'){
            title = "Exclusive Breastfeeding";
          }
          var data = test[0].contentWindow.dat;
          $('#table_modal').modal('open');
          $('#modal_body').append($('<h4/>').html(title));
          $('#modal_body').append('<table id="content_table" class="display"><table/>');
          buildHtmlTable('#content_table', data);
          $('#content_table').DataTable();
        });
      });
      $(".iuframe").load(function(){
        var iframe = $(this).contents();
        var test = $(this, top.document);
        iframe.find("canvas").click(function(){
          var title;
          if(test.attr('id') == 'birthrate'){
            $('#birth_rate_modal').modal('open');
          }
          else if (test.attr('id') == "births"){
            $('#births_modal').modal('open');
          }
          else if (test.attr('id') == 'contraceptive') {
            $('#contraceptive_modal').modal('open');
          }
          else if (test.attr('id') == 'philhealth_by_status'){
            $('#philhealth_by_status_modal').modal('open');
          }

        });
      });

    </script>

    <script type="text/javascript">
      function buildHtmlTable(selector, myList) {
        var columns = addAllColumnHeaders(myList, selector);
        var head;
        var row$;
        head = $('<thead/>');
        row$ = $('<tr/>');
        for(var i = 0; i < columns.length; i++){
          row$.append($('<th/>').html(columns[i]));
        }
        head.append(row$);
        $(selector).append(head);

        head = $('<tbody/>');
        for (var i = 0; i < myList.length; i++) {
          var row$ = $('<tr/>');
          for (var colIndex = 0; colIndex < columns.length; colIndex++) {
            var cellValue = myList[i][columns[colIndex]];
            if (cellValue == null) cellValue = "";
            row$.append($('<td/>').html(cellValue));
          }
          head.append(row$);
        }
        $(selector).append(head);
      }

      function addAllColumnHeaders(myList, selector) {
        var columnSet = [];
        var headerTr$ = $('<tr/>');

        for (var i = 0; i < myList.length; i++) {
          var rowHash = myList[i];
          for (var key in rowHash) {
            if ($.inArray(key, columnSet) == -1) {
              columnSet.push(key);
              headerTr$.append($('<th/>').html(key));
            }
          }
        }

        return columnSet;
      }
    </script>
    <script>
      $('#update_button').on("click", function () {
        var area = $("#area").val();
        var period = $("#period").val();
        var startDay = "01";
        var endDay = "01";
        if(period == "monthly"){
          var startMonth = $("#reporting_period_s_month").val();
          var startYear = $("#reporting_period_s_year").val();
          var endMonth = $("#reporting_period_e_month").val();
          var endYear = $("#reporting_period_e_year").val();
        //alert(startMonth+"\n"+startYear+"\n"+endMonth+"\n"+endYear);
      }
      if(period == "yearly"){
        var startMonth = 1;
        var startYear = $("#reporting_period_s_year").val();
        var endMonth = 12;
        var endYear = $("#reporting_period_e_year").val();
       // alert(startMonth+"\n"+startYear+"\n"+endMonth+"\n"+endYear);
     }

     if(period == "quarterly"){
      var startMonth = $("#reporting_period_s_quarter").val();
      var startYear = $("#reporting_period_e_quarter").val();
      var endMonth = parseInt($("#reporting_period_s_quarter").val())+2;
      var endYear = $("#reporting_period_e_quarter").val();
        //alert(startMonth+"\n"+startYear+"\n"+endMonth+"\n"+endYear);
      }

      if(period == "custom"){
        var start = $("#reporting_period_s_custom").val();
        var end = $("#reporting_period_e_custom").val();
        var startDate = start.split("-");
        var endDate = end.split("-");
        var startMonth = startDate[1];
        var startYear = startDate[0];
        var startDate = startDate[2];
        var endMonth = endDate[1];
        var endYear = endDate[0];
        var endDay = endDate[2];
        period = "monthly";
        //alert(startMonth+"\n"+startYear+"\n"+endMonth+"\n"+endYear);
      }
      // document.getElementById('deworming').contentWindow.location.reload();
      // $('#genders').attr('src', "<?=site_url()?>/data/genders/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + "01/" + period + "/" + area);
      document.getElementById("birthrate").src = "http://localhost/chits/index.php/data/birthrate/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay;
      document.getElementById("births").src = "http://localhost/chits/index.php/data/births/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay;
      document.getElementById("contraceptive").src = "http://localhost/chits/index.php/data/contraceptive/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay;
      document.getElementById("philhealth_by_status").src = "http://localhost/chits/index.php/data/philhealth_by_status/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay;
      //UPDATE DATATABLES
      $("#birth_rate_table").dataTable().fnDestroy();
      $("#birth_rate_table").dataTable({
        ajax: "http://localhost/chits/index.php/data/birth_rate_table/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay,
        "aaSorting": []
      })

      $("#births_table").dataTable().fnDestroy();
      $("#births_table").dataTable({
        ajax: "http://localhost/chits/index.php/data/births_table/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay,
        "aaSorting": []
      })

      $("#contraceptive_table").dataTable().fnDestroy();
      $("#contraceptive_table").dataTable({
        ajax: "http://localhost/chits/index.php/data/contraceptive_table/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay,
        "aaSorting": []
      })

      $("#philhealth_by_status_table").dataTable().fnDestroy();
      $("#philhealth_by_status_table").dataTable({
        ajax: "http://localhost/chits/index.php/data/philhealth_by_status_table/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay,
        "aaSorting": []
      })
      //END UPDATE DATATABLES

      //UPDATE CSV LINK
      document.getElementById("birth_rate_csv").href="http://localhost/chits/index.php/data/birth_rate_csv/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay;
      document.getElementById("births_csv").href="http://localhost/chits/index.php/data/births_table_csv/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay; 
      document.getElementById("contraceptive_csv").href="http://localhost/chits/index.php/data/contraceptive_csv/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay; 
      document.getElementById("philhealth_by_status_csv").href="http://localhost/chits/index.php/data/philhealth_by_status_table_csv/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay; 
      //END UPDATE CSV LINK

      $('#deworming').attr('src', "http://localhost/chits/index.php/data/deworming/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay);
      $('#fully_immunized').attr('src', "http://localhost/chits/index.php/data/fully_immunized/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay);
      $('#philhealth_archaic').attr('src', "http://localhost/chits/index.php/data/philhealth_archaic/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay);
      $('#exclusive_breastfeeding').attr('src', "http://localhost/chits/index.php/data/exclusive_breastfeeding/" + area + "/" + period + "/" + startYear + "-" + startMonth + "-" + startDay + "/" + endYear + "-" + endMonth + "-" + endDay);

    });
  </script>
  <script>
    $(document).ready(function(){

    })
  </script>

</body>

</html>