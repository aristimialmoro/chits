<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo base_url('assets/css/materialize.css')?>" type="text/css" rel="stylesheet" media="screen,projection"/>
	<style type="text/css">
	.chart-container {
		position: relative;
		margin: auto;
		    height: 500px;
    width: 500px;
	}
</style>
</head>
<body bgcolor="#FFFFFF">
	<!-- <div id="loader" class="loader"> -->
		
		<!-- <div id="chart-container"> -->
		<canvas id="mycanvas" width="400" height="400"></canvas>
		<!-- </div> -->


		<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script src="<?php echo base_url('assets/js/materialize.js')?>"></script>
		<script src="<?php echo base_url('assets/js/init.js')?>"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
		<script>
			var dat;
			$(document).ready(function(){
				$.ajax({
					url: "<?=site_url()?>/data/get_disaggregated_births",
					method: "GET",
					data: ({Start: '<?=$Start?>', End: '<?=$End?>', Period: '<?=$Period?>', Area: '<?=$Area?>'}),
					dataType: 'json',
					beforeSend: function(){
						$('#loader').show();
					},
					complete: function(){
						$('#loader').hide();
					},
					success: function(data) {
						dat = data;
						console.log("AJAX SUCCESS");
						console.log(data);
						var method = [];
						var count = [];
						var period = []
						var rowData = [];
						data.forEach(function(obj) {
							var tempArray = [];
							tempArray.push(obj.Method);
							if(method.indexOf(obj.Method) == -1){
								method.push(obj.Method);
							}
							tempArray.push(obj.Number);
							if(period.indexOf(obj.Period) == -1){
								period.push(obj.Period);
							}
							tempArray.push(obj.Period);
							rowData.push(tempArray);
						});
						var jsonText = "[";
						backgroundColor = ['#039be5',
			              '#ffa726',
			              '#66bb6a',
			              '#26a69a',
			              '#ffca28',
			              '#ab47bc',
			              '#d4e157',
			              '#26c6da',
			              '#ffee58',
			              '#00897b',
			              '#fb8c00'
			              ];
						for(var i = 0; i < method.length; i++){
							jsonText += "{\"label\": \"" + method[i] + "\", \"data\": [";
							var dataPerMethod = [];
							for (var j = 0; j < rowData.length; j++) {
							  if(rowData[j][0] == method[i]) dataPerMethod.push("\"" + rowData[j][1] + "\"");
							}
							jsonText += dataPerMethod + "], \"backgroundColor\": [";
							for(var k=0; k < period.length; k++){
								jsonText += "\"" + backgroundColor[i] + "\"";
								if(k<period.length-1) jsonText += ",";
							}
							jsonText += "]}";
							if(i<method.length-1) jsonText += ",";

						}
						jsonText += "]";
						var jsonParsed = JSON.parse(jsonText);
						console.log(jsonParsed);
						var chartdata = {
							labels: period,
							datasets : jsonParsed
						};

						var ctx = $("#mycanvas");

						var barGraph = new Chart(ctx, {
							type: 'bar',
							data: chartdata,
							options: {
								title: {
									display: true,
									text: 'Births Disaggregated by Method',
									fontSize: 16,
									fontFamily: "Roboto",
								},
								scales: {
									xAxes: [{
										scaleLabel:{
											stacked:true,
											display: true,
											labelString: "Period"
										}
									}],
									yAxes: [{
										ticks: {
											beginAtZero: true
										},
										scaleLabel:{
											stacked: true,
											display: true,
											labelString: "Count"
										}
									}]
								}
							}

						});
					},
					error: function(data) {
						console.log("AJAX ERROR");
						console.log(data);
					}
				});
			});
		</script>

	</body>
	</html>