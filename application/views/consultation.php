<html>
<head><title>Consultation</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <script src = "http://www.chartjs.org/samples/latest/utils.js"></script>
  <script src="http://www.chartjs.org/dist/2.7.2/Chart.bundle.js"></script> 
</head>
<body bgcolor="#FFFFFF">
  <canvas id="consultation" width="400" height="350"></canvas>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
  <script type="text/javascript">
    var ctx = document.getElementById("consultation").getContext("2d");
    var philhealth = new Chart(ctx, {
        type : "bar",
        data : <?= json_encode($chart_info) ?>, 
        options : {
          title: {
            display: true,
            text: "<?php echo $title ?>"
          },
          scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "<?= $x_axis ?>"
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "<?= $y_axis ?>"
                        },
                        ticks: {
                            beginAtZero: true
                        }
                    }]
          }
        }
    });
    </script>

     <!-- <?= json_encode($chart_info) ?> -->
 
</body>
</html>

