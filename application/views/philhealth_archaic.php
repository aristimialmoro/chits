<!DOCTYPE html>
<html>
<head>

</head>
<body bgcolor="#FFFFFF">

<!-- <div class="chart-container"> -->
<canvas id="mycanvas" width="400" height="400"></canvas>
<!-- </div> -->

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<script>
  var dat;
  $(document).ready(function(){
    $.ajax({
      url: "<?=site_url() . '/data/get_philhealth_archaic';?>",
      method: "GET",
      data: ({Start: '<?=$Start?>', End: '<?=$End?>', Period: '<?=$Period?>', Area: '<?=$Area?>'}),
      dataType: 'json',
      beforeSend: function(){
        $('#loader').show();
      },
      complete: function(){
        $('#loader').hide();
      },
      success: function(data) {
        dat = data;
        console.log("Success");
        console.log(data);
        var role = [];
        var count = [0,0,0,0,0,0,0,0,0,0,0];
        data.forEach(function(obj) {
          switch(obj.value){
            case "16":
            count[0]+=1;
            break;
            case "17":
            count[1]+=1;
            break;
            case "18":
            count[2]+=1;
            break;
            case "19":
            count[3]+=1;
            break;
            case "299":
            count[4]+=1;
            break;
            case "7334":
            count[5]+=1;
            break;
            case "7335":
            count[6]+=1;
            break;
            case "7336":
            count[7]+=1;
            break;
            case "7337":
            count[8]+=1;
            break;
            case "8195":
            count[9]+=1;
            case "10008":
            count[10]+=1;
            break;
            default:break;

          }

        });
         if(data.length!=0){
          var chartdata = {
            labels: ['CCT-NATIONAL','LGU','IPP','EMPLOYER','other non-coded','Barangay','MUNICIPALITY/CITY','PROVINCE','REGION','PHIC indigent,LGU','PHIC OFW'],
            datasets : [
            {
              data: role,
              backgroundColor: ['#039be5',
              '#ffa726',
              '#66bb6a',
              '#26a69a',
              '#ffca28',
              '#ab47bc',
              '#d4e157',
              '#26c6da',
              '#ffee58',
              '#00897b',
              '#fb8c00'],
              data: count
            }
            ]
          };
        }else{
          count = [1,0,0,0,0,0,0,0,0,0,0];
          var chartdata = {
            labels: ['None'],
            datasets : [
            {
             data: role,
             backgroundColor : ['rgba(232,232,232)'],
             data: count
           }
           ]
         };

       }

       var ctx = $("#mycanvas");

       var barGraph = new Chart(ctx, {
        type: 'pie',
        data: chartdata,
        options: {
          title: {
            display: true,
            text: 'Number of Philhealth Beneficiaries (Disaggregated by Roles)',
            fontSize: 16,
            fontFamily: "Roboto",
          }
        }
      });
     },
     error: function(data) {
      console.log("Error");
      console.log(data);
    }
  });
  });
</script>
</body>
</html>