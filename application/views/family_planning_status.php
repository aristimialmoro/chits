<html>
<head><title>Contraceptive and family planning users: Status</title></head>
<body bgcolor="#FFFFFF">
    <canvas id="myChart" width="400" height="350"></canvas>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
<script>

		    var barChartData = <?=json_encode($chart_info)?>;
			var ctx = document.getElementById('myChart').getContext('2d');
			var family_planning_status_chart = new Chart(ctx, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Contraceptive and family planning users: Status'
					},
					tooltips: {
					mode: 'index',
				    intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							scaleLabel: {
	                            display: true,
	                            labelString: '<?= $x_axis_label?>'
	                        },
							stacked: true
						}],
						yAxes: [{
							scaleLabel: {
	                            display: true,
	                            labelString: '<?= $y_axis_label?>'
	                        },
							stacked: true
						}]
					}
				}
			});
</script>
</html>
