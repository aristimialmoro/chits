<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?=$chart_info['short_title']?></title>
</head>
<body bgcolor="#FFFFFF">
    <canvas id="births_attended_chart" width="400" height="400"></canvas>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script>
        var canvas_context = document.getElementById("births_attended_chart").getContext("2d");
        var data = <?=json_encode($chart_info['data'])?>;
        var chart = new Chart(canvas_context, {
            type : "bar",
            data : data,
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: "<?=$chart_info['long_title']?>"
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "<?=$chart_info['x_axis']?>"
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: "<?=$chart_info['y_axis']?>"
                        },
                        ticks: {
                            beginAtZero: true,
                            callback: function(value){if(Number.isInteger(value)){return value}}
                        }
                    }]
                }
            } 
        })
    </script>
</body>
</html>