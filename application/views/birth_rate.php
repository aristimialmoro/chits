<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src = "http://www.chartjs.org/samples/latest/utils.js"></script>
	<script src="http://www.chartjs.org/dist/2.7.2/Chart.bundle.js"></script> 
</head>
<body bgcolor="#FFFFFF">

    <canvas id="birth_rate" width="400" height="400"></canvas>
  
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
  <script type="text/javascript">
  
    var ctx = document.getElementById("birth_rate");
    var data = <?=json_encode($dataset)?>;
    var birth_rate = new Chart(ctx, {
        type : "line",
        data : data,
        options : {
        	title: {
						display: true,
						text: '<?= $title?>'
			},
            scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: '<?= $x_axis_label?>'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: '<?= $y_axis_label?>'
                        },
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
        }
    });


    </script>

</body>
</html>