<?php
/**
 * Model Functions for Prequels
 *
 * @author Reena Myka De Guzman <[<email address>]>
 * @author Harold R. Mansilla <hrmansilla08@gmail.com>
 * @author Lorenz Timothy Barco Ranera <lbranera@up.edu.ph>
 */
class Preq_model extends CI_Model {


  /**
   * NUMBER OF CONSULTATION PER HEALTH CENTER MODEL FUNCTIONS ====================================================
   * @author Lorenz Timothy Barco Ranera <lbranera@up.edu.ph>
   */

	public function get_consultation_warehouse(){
      $this->load->database();
      $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos");
      for($i=0; $i<count($areas); $i++){
         $result = $this->db->query('SELECT name, count(*) FROM `'.$areas[$i].'_consultation` GROUP BY name')->result_array();
         
         $warehouse[$i] = $result;
      }

       return $warehouse;
    }

    /** END OF NUMBER OF CONSULTATIONS PER HEALTH CENTER  ========================================================*/







    /**
     * START OF DISAGGREGATED BIRTHS FUNCTIONS ==================================================================
     * @author Harold R. Mansilla <hrmansilla08@gmail.com>
     */
    
    public function set_all_births_view($Area){
      $this->load->database();
      $areas = array('kvhc','tangos','sipacalmacen','phase2area1');
      $db_name = "chits_" . $Area;
      $create_view = "CREATE OR REPLACE VIEW all_births_view AS ";
      for ($i = 0; $i < sizeof($areas); $i++) {
        $create_view .= "(SELECT * FROM " . "chits_" . $areas[$i] . ".obs WHERE " . "chits_" . $areas[$i] . ".obs.concept_id = 189)";
        if($i < sizeof($areas) - 1) $create_view .= " UNION ALL ";
      }
        //SELECT * FROM `obs` WHERE `concept_id` IN (189,190,191,192,193,194)  ORDER BY `obs`.`concept_id`  ASC
      $this->db->query($create_view);
    }
    
    public function get_disaggregated_births($Start, $End, $Intervals, $Area){
      $this->load->database();
      $filter = "obs_datetime BETWEEN '" . $Start . "' AND '" . $End . "'";
      $fields="";
      $grouping="";
      if (strcmp($Intervals, "yearly") == 0){
        $fields = "value_text AS Method, COUNT(value_text) AS Number, DATE_FORMAT(obs_datetime, '%Y') AS Period";
        $grouping = "value_text, YEAR(all_births_view.obs_datetime)";
      }
      else if (strcmp($Intervals, "monthly") == 0 || strcmp($Intervals, "quarterly") == 0){
        if (strcmp($Intervals, "monthly") == 0){
          $fields = "value_text AS Method, COUNT(value_text) AS Number, CONCAT(MONTHNAME(obs_datetime), ' ' ,DATE_FORMAT(obs_datetime, '%Y')) AS Period";
          $grouping = "value_text, YEAR(all_births_view.obs_datetime), MONTH(all_births_view.obs_datetime)";
        }
        else if (strcmp($Intervals, "quarterly") == 0) {
          $fields = "value_text AS Method, COUNT(value_text) AS Number, CONCAT('Q', QUARTER(obs_datetime), ' ', YEAR(obs_datetime)) AS Period";
          $grouping = "value_text, YEAR(all_births_view.obs_datetime), QUARTER(all_births_view.obs_datetime)";
        }
      }
      $this->db->select($fields, FALSE)
      ->from("all_births_view")
      ->group_start()
      ->where($filter)
      ->group_end()
      ->group_by($grouping);
      $res = $this->db->get()->result();

      $ProperRes = $this->createNewRes_disaggregated_births($res, $Start, $End, $Intervals);
      return $ProperRes;
    }
    

    private function createNewRes_disaggregated_births($res, $Start, $End, $Intervals){
      $NewRes = array();
      $span = array();
      if($Intervals == "monthly"){
        $span = $this->getMonths($Start,$End);
      }else if($Intervals == "quarterly"){
        $span = $this->getQuarters($Start,$End);
      }else if($Intervals == "yearly"){
        $span = $this->getYears($Start, $End);
      }
      $Methods = array();
      foreach($res as $row){
        if(!in_array($row->Method, $Methods)){
          array_push($Methods, $row->Method);
        }
      }
      foreach ($Methods as $method) {
        foreach ($span as $period) {
          $newRow = new stdClass();
          $newRow->Period = $period;
          $newRow->Method = $method;
          foreach($res as $row){
            if(strcmp($row->Method, $method) == 0){
              if(strcasecmp($row->Period, $period) == 0){
                @$newRow->Number = $row->Number;
                break;
              }
              else{
                @$newRow->Number = "0";
              }
            }
          }
          array_push($NewRes, $newRow);
        } 
      }
      return $NewRes;
    }
    /* END OF DISAGGREGATED BIRTHS FUNCTIONS ===========================================================================*/







    
    /**
     * START OF POPULATION BY AGE BRACKET =============================================================================
     * @author Reena Myka De Guzman <[<email address>]>
     */

    public function set_population_by_age_bracket_view($Area){  
      $this->load->database();
      $areas = array('kvhc','tangos','sipacalmacen','phase2area1');
      // $db_name = "chits_" . $Area;
      // $create_view = "CREATE OR REPLACE VIEW all_births_view AS ";
      // for ($i = 0; $i < sizeof($areas); $i++) {
      //   $create_view .= "(SELECT * FROM " . "chits_" . $areas[$i] . ".obs WHERE " . "chits_" . $areas[$i] . ".obs.concept_id = 189)";
      //   if($i < sizeof($areas) - 1) $create_view .= " UNION ALL ";
      // }
      //   //SELECT * FROM `obs` WHERE `concept_id` IN (189,190,191,192,193,194)  ORDER BY `obs`.`concept_id`  ASC
      // $this->db->query($create_view);
      $db_name = "chits_" . $Area;
      $create_view = "CREATE OR REPLACE VIEW disaggregate_population_view AS ";
      for ($i = 0; $i < sizeof($areas); $i++) {
        $create_view .= "(SELECT DISTINCT obs.person_id,  person.birthdate, TIMESTAMPDIFF(YEAR, person.birthdate, obs_datetime) AS age FROM chits_" . $areas[$i] . ".obs JOIN " . "chits_" . $areas[$i]  . ".person ON chits_" . $areas[$i] . ".obs.person_id = " . "chits_" . $areas[$i] . ".person.person_id)";
        if($i < sizeof($areas) - 1) $create_view .= " UNION ALL ";
            //$create_view = "SELECT DISTINCT obs.person_id, FLOOR((DATEDIFF(obs_datetime, STR_TO_DATE(person.birthdate, '%Y-%m-%d'))/365) AS age FROM obs JOIN person ON obs.person_id = person.person_id)";"
      }
      $this->db->query($create_view);
    }


    public function get_populationbyagebracket2($Start, $End, $Intervals, $Area){
      $this->load->database();
      $fields="";
      $grouping="";
      if (strcmp($Intervals, "yearly") == 0){
        $fields = "COUNT(age) AS Number, DATE_FORMAT(birthdate, '%Y') AS Period";
        $grouping = "YEAR(birthdate)";
      }
      else if (strcmp($Intervals, "monthly") == 0 || strcmp($Intervals, "quarterly") == 0){
        if (strcmp($Intervals, "monthly") == 0){
          $fields = "COUNT(age) AS Number, CONCAT(MONTHNAME(birthdate), ' ' ,DATE_FORMAT(birthdate, '%Y')) AS Period";
          $grouping = "YEAR(birthdate), MONTH(birthdate)";
        }
        else if (strcmp($Intervals, "quarterly") == 0) {
          $fields = "COUNT(age) AS Number, CONCAT('Q', QUARTER(birthdate), ' ', YEAR(birthdate)) AS Period";
          $grouping = "YEAR(birthdate), QUARTER(birthdate)";
        }
      }
      
      $query = "SELECT " . $fields . " FROM `disaggregate_population_view` GROUP BY " . $grouping;
      $q = $this->db->query($query);
      $res = $q->result();   

      // $ProperRes = $this->createNewRes_deworming($res, $Start, $End, $Intervals);

      return $res;   
    }

    /** END OF POPULATION BY AGE BRACKET ========================================================================= */
    







    /**
     * AUXILLIARY FUNCTIONS ======================================================================================
     */

    public function getMonths($Start, $End){
	    $startD = new DateTime($Start);
	    $endD = new DateTime($End);
	    $nEndD = date_add($endD, date_interval_create_from_date_string('1 month'));
	    $interval = DateInterval::createFromDateString('1 Month');
	    $period   = new DatePeriod($startD, $interval, $nEndD);
	    $span = array();

	    foreach ($period as $dt) {
	      $span[] = $dt->format('F Y');
	    }


	    return $span;
    }


    public function getQuarters($Start, $End){
        $startD = new DateTime($Start);
        $endD = new DateTime($End);
        $nEndD = date_add($endD, date_interval_create_from_date_string('1 month'));
        $interval = DateInterval::createFromDateString('1 Month');
        $period   = new DatePeriod($startD, $interval, $nEndD);
        $span = array();

        foreach ($period as $dt) {
          $temp = ceil((int)$dt->format('m')/3);
          if (!in_array("Q".$temp." ".$dt->format('Y'), $span)) {
            $span[] = "Q" . $temp . " " . $dt->format('Y');
          }
        }
        return $span;
    }

    public function getYears($Start, $End){
        $startD = new DateTime($Start);
        $endD = new DateTime($End);
        $nEndD = date_add($endD, date_interval_create_from_date_string('1 month'));
        $interval = DateInterval::createFromDateString('1 Year');
        $period   = new DatePeriod($startD, $interval, $nEndD);
        $span = array();

        foreach ($period as $dt) {
          $span[] = $dt->format('Y');
        }


        return $span;
    }


}
?>