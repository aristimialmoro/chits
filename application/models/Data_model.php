<?php
/**
 *    Model Functions for Indicators
 *    
 *    @author Rose Angelica S. Carandang <rscarandang@up.edu.ph>
 *    @author Jeremiah Sean S. De Guzman <jsdeguzman6@up.edu.ph>
 *    @author Reena Myka De Guzman <[<email address>]>
 *    @author Ced Rick C. Flores <ccflores@up.edu.ph>
 *    @author John Robinson Francisco <jsfrancisco5@up.edu.ph>
 *    @author Harold R. Mansilla <hrmansilla08@gmail.com>
 *    @author Virgilio M. Mendoza III <vmmendoza1@up.edu.ph>
 *    @author Waleed C. Occidental <woccidental@up.edu.ph>
 *    @author Sean Louie Pineda <pinedaseanlouie@gmai>
 *    @author Lorenz Timothy Barco Ranera <lbranera@up.edu.ph>
 */

class Data_model extends CI_Model {
    

 /**
  *    BIRTH RATE MODEL FUNCTIONS ===========================================================================
  *
  *    @author Rose Angelica S. Carandang <rscarandang@up.edu.ph>
  */
    
  
  public function get_birthrates($area, $start_date, $end_date, $period){
        $birthrates = $this->get_livebirths($area, $start_date, $end_date, $period);
        $populations = $this->get_population($area, $start_date, $end_date, $period);

        $curr_pop = 0;

        foreach($birthrates as &$birthrate){

            $curr = current($populations);
            while($curr['birthdate'] <= $birthrate['birth_date'] && $curr != null){
                $curr_pop += $curr['population'];
                $curr = next($populations);
            }

            $rate = ($birthrate['total'] / $curr_pop)*100;
            $birthrate['total'] = number_format((float)$rate, 2, '.', '');
        }

        return $birthrates;
  }

  public function get_livebirths($area, $start_date, $end_date, $period){
        $this->load->database();

        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos");
        $columns = "";
        $grouping = "";
        $ph_query = "";
        $result = "";


        if (strcasecmp($period, "yearly") === 0){
            $start_date = date("Y", strtotime($start_date)) . "-01-01 00:00:00";
            $end_date = date("Y", strtotime($end_date)) . "-12-31 23:59:59";

            $columns = "YEAR(`birth_date`) as `birth_year`, COUNT(*) as `total`, `birth_date`";
            $grouping = "YEAR(`birth_date`)";
            $ph_query = "SELECT `birth_year`, SUM(`total`) AS `total`, `birth_date`";

        }elseif (strcasecmp($period, "monthly") === 0) {
            $start_date .= " 00:00:00";
            $end_date .= " 23:59:59";

            $columns = "MONTH(`birth_date`) as `birth_month`, COUNT(*) as `total`, YEAR(`birth_date`) as `birth_year`, `birth_date`";
            $grouping = "YEAR(`birth_date`), MONTH(`birth_date`)";
            $ph_query = "SELECT `birth_month`, SUM(`total`) AS `total`, `birth_year`, `birth_date`";

        }elseif(strcasecmp($period, "quarterly") === 0){
            $columns = "QUARTER(`birth_date`) AS `birth_quarter`, COUNT(*) as `total`, YEAR(`birth_date`) as `birth_year`, `birth_date`";
            $grouping = "YEAR(`birth_date`), QUARTER(`birth_date`)";
            $ph_query = "SELECT `birth_quarter`, SUM(`total`) AS `total`, `birth_year`, `birth_date`";
        }

        $start = date('Y-m-d H:i:s', strtotime($start_date));
        $end = date('Y-m-d H:i:s',strtotime($end_date));

        if(strcasecmp($area, "philippines") === 0){

          $db_queries = array();
          foreach($areas as $area){

            $query = "SELECT ".$columns." FROM `".$area."_v_livebirths` WHERE `birth_date` >= '".$start."' AND `birth_date` <= '".$end."'"; 

            array_push($db_queries, $query);
          }
          $union_query = $ph_query." FROM( ";
          $size = sizeof($db_queries);
          for($i = 0; $i < $size; $i++){
            if($i == $size-1){
              $union_query .= $db_queries[$i];
            }else{
              $union_query .= $db_queries[$i]." UNION ";
            }
          }
          
          $union_query .= ") AS LB_TABLE WHERE `birth_year` IS NOT NULL GROUP BY ".$grouping;
          $result = $this->db->query($union_query)->result_array();

        }else{
          $this->db->from($area . '_v_livebirths');
          $this->db->select($columns);
          $this->db->where("birth_date >=", $start);
          $this->db->where("birth_date <=", $end);
          $this->db->group_by($grouping);

          $result =  $this->db->get()->result_array();
        }

        return $result;
   }



   public function get_population($area, $start_date, $end_date, $period){
        $this->load->database();

        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos");        
        $columns = "";
        $grouping = "";
        $ph_query = "SELECT `birthdate`, SUM(`population`) AS  `population` ";
        $result = "";

        if (strcasecmp($period, "yearly") === 0){
            $start_date = date("Y", strtotime($start_date)) . "-01-01 00:00:00";
            $end_date = date("Y", strtotime($end_date)) . "-12-31 23:59:59";

            $columns = "`birthdate`, COUNT(*) as `population`";
            $grouping = "YEAR(`birthdate`)";

        }elseif (strcasecmp($period, "monthly") === 0) {
            $start_date .= " 00:00:00";
            $end_date .= " 23:59:59";

            $columns = "`birthdate`, COUNT(*) as `population`";
            $grouping = "YEAR(`birthdate`), MONTH(`birthdate`)";

        }elseif(strcasecmp($period, "quarterly") === 0){
            $columns = "`birthdate`, COUNT(*) as `population`";
            $grouping = "YEAR(`birthdate`), QUARTER(`birthdate`)";
        }

        $start = date('Y-m-d H:i:s', strtotime($start_date));
        $end = date('Y-m-d H:i:s',strtotime($end_date));

        if(strcasecmp($area, "philippines") === 0){

          $db_queries = array();
          foreach($areas as $area){

            $query = "SELECT ".$columns." FROM `".$area."_v_population` WHERE `birthdate` <= '".$end."'"; 

            array_push($db_queries, $query);
          }
          $union_query = $ph_query." FROM( ";
          $size = sizeof($db_queries);
          for($i = 0; $i < $size; $i++){
            if($i == $size-1){
              $union_query .= $db_queries[$i];
            }else{
              $union_query .= $db_queries[$i]." UNION ";
            }
          }
          
          $union_query .= ") AS POP_TABLE GROUP BY ".$grouping;
          $result = $this->db->query($union_query)->result_array();

        }else{
          $this->db->from($area . '_v_population');
          $this->db->select($columns);
          $this->db->where("birthdate <=", $end);
          $this->db->group_by($grouping);

          $result =  $this->db->get()->result_array();

        }

        return $result;
  }
  /** BIRTH RATE MODEL FUNCTIONS ====================================================================================  */
 








/**
 *  BIRTHS ATTENDED BY SKILLED HEALTH PROFESSIONALS MODEL FUNCTIONS ==================================================
 *
 *  @author Jeremiah Sean S. De Guzman <jsdeguzman6@up.edu.ph>
 */


   public function get_births_warehouse($area, $start_date, $end_date, $period){
        $this->load->database();

        $start_year = date("Y", strtotime($start_date));
        $end_year = date("Y", strtotime($end_date));
        
        $areas = array();

        if(strcasecmp($area, "philippines") === 0){
            $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos");
        }else{
            $areas = array($area);
        }
        
        $areas_size = count($areas);

        $final_query = "";

        foreach($areas as $index => $single_area){
            $filters = array("YEAR(`birth_date`) >" => $start_year, "YEAR(`birth_date`) <" => $end_year);
            $second_filter = array("YEAR(`birth_date`) = " => $start_year);
            $third_filter = array("YEAR(`birth_date`) = " => $end_year);

            $count_cols = array("SUM(`profession` LIKE '%doctor%' OR `profession` LIKE '%specialist%') AS `Doctors`",
                                "SUM(`profession` LIKE '%midwife%') AS `Midwives`",
                                "SUM(`profession` LIKE '%nurse%') AS `Nurses`");

            $group_cols = array("YEAR(`birth_date`) AS `birth_year`");

            $groupings = array("YEAR(`birth_date`)");

            if(strcasecmp($period, "monthly") === 0){
                $start_month = date("n", strtotime($start_date));
                $end_month = date("n", strtotime($end_date));

                $group_cols[] = "MONTH(`birth_date`) AS `birth_month`";
                $groupings[] = "MONTH(`birth_date`)";

                $second_filter['MONTH(`birth_date`) >='] = $start_month;
                $third_filter['MONTH(`birth_date`) <='] = $end_month;
            }

            if(strcasecmp($period, "quarterly") === 0){
                $start_quarter = ceil((date("n", strtotime($start_date)))/3);
                $end_quarter = ceil((date("n", strtotime($end_date)))/3);

                $group_cols[] = "QUARTER(`birth_date`) AS `birth_quarter`";
                $groupings[] = "QUARTER(`birth_date`)";

                $second_filter['QUARTER(`birth_date`) >='] = $start_quarter;
                $third_filter['QUARTER(`birth_date`) <='] = $end_quarter;
            }

            $this->db->select($group_cols, FALSE);
            $this->db->select($count_cols, FALSE);

            $this->db->from($single_area . '_v_births_attended');

            $this->db->group_start();
            $this->db->where($filters, NULL, FALSE);
            $this->db->group_end();

            $this->db->or_group_start();
            $this->db->where($second_filter, NULL, FALSE);
            $this->db->group_end();

            $this->db->or_group_start();
            $this->db->where($third_filter, NULL, FALSE);
            $this->db->group_end();

            if(strcasecmp($period, "monthly") === 0 || strcasecmp($period, "quarterly") === 0){
                $this->db->where("has_month", 1);
            }

            $this->db->group_by($groupings, FALSE);

            $final_query .= $this->db->get_compiled_select();

            if($index + 1 < $areas_size){
                $final_query .= " UNION ";
            }
        }

        return $this->db->query($final_query)->result_array();
   }
  






  /**
   * CONTRACEPTIVE AND FAMILY PLANNING USERS: STATUS MODEL FUNCTIONS =================================================
   * 
   * @author Ced Rick C. Flores <ccflores@up.edu.ph>
   */
  
  public function get_family_planning_status($area, $start_date, $end_date, $period){
      $this->load->database();
      $subqueries = array();
      if(strcasecmp($area, "philippines") === 0){
           $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos");
      }else{
           $areas = array($area);
      }

      $old_start_date = $start_date;
      $old_end_date = $end_date;
      foreach ($areas as $area) {
          $start_date = $old_start_date;
          $end_date = $old_end_date;
          if (strcasecmp($period, "yearly") === 0){
             $start_date = date("Y", strtotime($start_date)) . "-01-01 00:00:00";
             $end_date = date("Y", strtotime($end_date)) . "-12-31 23:59:59";
             $query = "SELECT YEAR(obs_datetime) AS 'year',
                        SUM(CASE WHEN family_planning_status LIKE '%(CU)%' THEN 1 ELSE 0 END) cu_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(CC)%' THEN 1 ELSE 0 END) cc_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(RS)%' THEN 1 ELSE 0 END) rs_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(CM)%' THEN 1 ELSE 0 END) cm_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(LU)%' THEN 1 ELSE 0 END) lu_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(NA)%' THEN 1 ELSE 0 END) na_count
                        FROM " . $area . "_v_status_family_planning_users WHERE `obs_datetime` BETWEEN '" . $start_date . "' AND '" . $end_date . "'
                        GROUP BY YEAR(obs_datetime)
                        ORDER BY obs_datetime";

            if(sizeof($areas) !== 1){
                $subqueries[] = '('.$query.')';
                $sub_sql = implode(' UNION ALL ', $subqueries);
                $sql_query = "SELECT year, SUM(cu_count) AS cu_count, SUM(cc_count) AS cc_count, SUM(rs_count) AS rs_count, SUM(cm_count) AS cm_count, SUM(lu_count) AS lu_count, SUM(na_count) AS na_count FROM"
                        . "(".$sub_sql.")"." AS x group by year ORDER BY year";
            }else{
                $sql_query = $query;
            }

          }elseif (strcasecmp($period, "monthly") === 0) {
              $start_date .= " 00:00:00";
              $end_date .= " 23:59:59";
              $query = "SELECT YEAR(obs_datetime) AS 'year', MONTH(obs_datetime) AS 'month',
                        SUM(CASE WHEN family_planning_status LIKE '%(CU)%' THEN 1 ELSE 0 END) cu_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(CC)%' THEN 1 ELSE 0 END) cc_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(RS)%' THEN 1 ELSE 0 END) rs_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(CM)%' THEN 1 ELSE 0 END) cm_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(LU)%' THEN 1 ELSE 0 END) lu_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(NA)%' THEN 1 ELSE 0 END) na_count
                        FROM " . $area . "_v_status_family_planning_users WHERE `obs_datetime` BETWEEN '" . $start_date . "' AND '" . $end_date . "'
                        GROUP BY YEAR(obs_datetime), MONTH(obs_datetime)
                        ORDER BY obs_datetime";
            if(sizeof($areas) !== 1){
                $subqueries[] = '('.$query.')';
                $sub_sql = implode(' UNION ALL ', $subqueries);
                $sql_query = "SELECT year, month, SUM(cu_count) AS cu_count, SUM(cc_count) AS cc_count, SUM(rs_count) AS rs_count, SUM(cm_count) AS cm_count, SUM(lu_count) AS lu_count, SUM(na_count) AS na_count FROM"
                        . "(".$sub_sql.")"." AS x group by year, month ORDER BY year, month";
            }else{
                $sql_query = $query;
            }
          }elseif(strcasecmp($period, "quarterly") === 0){
              $start_date .= " 00:00:00";
              $end_date .= " 23:59:59";
              $query = "SELECT YEAR(obs_datetime) AS 'year', QUARTER(obs_datetime) AS 'quarter',
                        SUM(CASE WHEN family_planning_status LIKE '%(CU)%' THEN 1 ELSE 0 END) cu_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(CC)%' THEN 1 ELSE 0 END) cc_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(RS)%' THEN 1 ELSE 0 END) rs_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(CM)%' THEN 1 ELSE 0 END) cm_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(LU)%' THEN 1 ELSE 0 END) lu_count,
                        SUM(CASE WHEN family_planning_status LIKE '%(NA)%' THEN 1 ELSE 0 END) na_count
                        FROM " . $area . "_v_status_family_planning_users WHERE `obs_datetime` BETWEEN '" . $start_date . "' AND '" . $end_date . "'
                        GROUP BY YEAR(obs_datetime), QUARTER(obs_datetime)
                        ORDER BY obs_datetime";
            if(sizeof($areas) !== 1){
                $subqueries[] = '('.$query.')';
                $sub_sql = implode(' UNION ALL ', $subqueries);
                $sql_query = "SELECT year, quarter, SUM(cu_count) AS cu_count, SUM(cc_count) AS cc_count, SUM(rs_count) AS rs_count, SUM(cm_count) AS cm_count, SUM(lu_count) AS lu_count, SUM(na_count) AS na_count FROM"
                        . "(".$sub_sql.")"." AS x group by year, quarter ORDER BY year, quarter";
            }else{
                $sql_query = $query;
            }
          }
      }

      $result_query = $this->db->query($sql_query);
      return $result_query->result_array();
   }

   /** END OF CONTRACEPTIVE AND FAMILY PLANNING USERS: STATUS MODEL FUNCTIONS ==================================== */








   
   /**
    * NUMBER OF PHILHEALTH: DISAGGREGATED BY STATUS MODEL FUNCTIONS ==============================================
    * @author Lorenz Timothy Barco Ranera <lbranera@up.edu.ph>
    */

   public function get_philhealth_by_status($area, $start_date, $end_date, $period){

        $this->load->database();
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos");

        if(strcasecmp($period, "yearly") === 0){
          $start_date = date("Y",strtotime($start_date))."-01-01";
          $sdate = strtotime($start_date);
          $temp = date("Y-m-d", strtotime("+1 year",$sdate));
          $tmp = strtotime($temp);
          $end = date("Y-m-d", strtotime("-1 day",$tmp));
        }else if(strcasecmp($period, "monthly") === 0){
          $start_date = date("Y-m",strtotime($start_date))."-01";
          $sdate = strtotime($start_date);
          $temp = date("Y-m-d", strtotime("+1 month",$sdate));
          $tmp = strtotime($temp);
          $end = date("Y-m-d", strtotime("-1 day",$tmp));
        }else if(strcasecmp($period, "quarterly") === 0){
          $start_date = $this->data_model->getStartForQuarter($start_date);
          $sdate = strtotime($start_date);
          $temp = date("Y-m-d", strtotime("+3 ymonth",$sdate));
          $tmp = strtotime($temp);
          $end = date("Y-m-d", strtotime("-1 day",$tmp));
        }

       $result = array();
       while($this->data_model->compare_date($start_date,$end_date)){

        if(strcasecmp($period, "yearly") === 0){
           $column = date("Y", strtotime($start_date));
           $column_name = "Year";
           $sdate = strtotime($start_date);
           $new_start = date("Y-m-d", strtotime("+1 year",$sdate));
           $nstart = strtotime($new_start);
           $new_end = date("Y-m-d", strtotime("+1 year",$nstart));
           $nend = strtotime($new_end);
           $new_end = date("Y-m-d", strtotime("-1 day",$nend));
        }else if(strcasecmp($period, "monthly") === 0){
           $year = date("Y", strtotime($start_date));
           $column = date("m", strtotime($start_date));
           $column = $this->data_model->getMonthName($column);
           $column = $year." ".$column;
           $column_name = "Month";
           $sdate = strtotime($start_date);
           $new_start = date("Y-m-d", strtotime("+1 month",$sdate));
           $nstart = strtotime($new_start);
           $new_end = date("Y-m-d", strtotime("+1 month",$nstart));
           $nend = strtotime($new_end);
           $new_end = date("Y-m-d", strtotime("-1 day",$nend));
        }else if(strcasecmp($period, "quarterly") === 0){
           $year = date("Y", strtotime($start_date));
           $column = $this->data_model->getQuarter($start_date);
           $column_name = "Quarter";
           $column = $year." Q".$column;
           $sdate = strtotime($start_date);
           $new_start = date("Y-m-d", strtotime("+3 month",$sdate));
           $nstart = strtotime($new_start);
           $new_end = date("Y-m-d", strtotime("+3 month",$nstart));
           $nend = strtotime($new_end);
           $new_end = date("Y-m-d", strtotime("-1 day",$nend));
        }

        if(strcasecmp($area, "philippines") === 0){
          $area = $areas[0];
          $my_query ="Select '".$column."' AS `".$column_name."`, (SELECT  COUNT(*) FROM `" . $area . "_v_philhealth_expiration_dates_person` AS ph WHERE STR_TO_DATE(ph.value, '%m/%d/%Y') > '".$end."') Active, (SELECT COUNT(*) FROM `" . $area . "_v_philhealth_expiration_dates_person` AS ph WHERE STR_TO_DATE(ph.value, '%m/%d/%Y') < '".$start_date."') Expired, ( SELECT COUNT(*) FROM `" . $area . "_v_philhealth_expiration_dates_person` AS ph WHERE STR_TO_DATE(ph.value, '%m/%d/%Y') BETWEEN '".$start_date."' AND '".$end."') 'Expired during period'";
          $a_result = (array) $this->db->query($my_query)->result_array();
          for($i=1; $i<count($areas); $i++){
            $area = $areas[$i];
            $temp_result = (array) $this->db->query($my_query)->result_array();

            $a_result[0]['Active']  = $a_result[0]['Active'] + $temp_result[0]['Active'];
            $a_result[0]['Expired']  = $a_result[0]['Expired'] + $temp_result[0]['Expired'];
            $a_result[0]['Expired during period']  = $a_result[0]['Expired during period'] + $temp_result[0]['Expired during period'];

          }

        }else{
          $my_query ="Select '".$column."' AS `".$column_name."`, (SELECT  COUNT(*) FROM `" . $area . "_v_philhealth_expiration_dates_person` AS ph WHERE STR_TO_DATE(ph.value, '%m/%d/%Y') > '".$end."') Active, (SELECT COUNT(*) FROM `" . $area . "_v_philhealth_expiration_dates_person` AS ph WHERE STR_TO_DATE(ph.value, '%m/%d/%Y') < '".$start_date."') Expired, ( SELECT COUNT(*) FROM `" . $area . "_v_philhealth_expiration_dates_person` AS ph WHERE STR_TO_DATE(ph.value, '%m/%d/%Y') BETWEEN '".$start_date."' AND '".$end."') 'Expired during period'";
          $a_result = (array) $this->db->query($my_query)->result_array();
        }

        $result = array_merge($result, $a_result);
        $start_date = $new_start;
        $end = $new_end;
       }


       return $result;
   }
   
   /** END OF NUMBER OF PHILHEALTH: DISAGGREGATED BY STATUS MODEL FUNCTIONS ========================================= */









  /**
   * FULLY IMMUNIZED CHILDREN MODEL FUNCTIONS =========================================================================
   * 
   * @author Harold R. Mansilla <hrmansilla08@gmail.com>
   */
    

    public function set_immunizations_view($Area){ //Selects all immunizations from obs table along with the person id and birthdate of the person involved
            // $this->db->db_select($Area);
      $this->load->database();
      $db_name = "chits_" . $Area;
            //SPECIAL CASE: WHOLE PHILIPPINES
      $create_view = "CREATE OR REPLACE VIEW immunizations_view_" . $Area . " AS (SELECT DISTINCT " . $db_name . ".obs.obs_id," . $db_name . ".obs.concept_id," . $db_name . ".obs.obs_datetime," . $db_name . ".person.person_id," . $db_name . ".person.birthdate FROM " . $db_name . ".obs JOIN " . $db_name . ".person ON " . $db_name . ".obs.person_id = " . $db_name. ".person.person_id" . " WHERE " . $db_name . ".obs.concept_id = 7578 AND obs.obs_datetime BETWEEN person.birthdate AND Date_Add(person.birthdate, INTERVAL 100 YEAR))";
            //CREATE VIEW immunizations_view_kvhc AS (SELECT chits_kvhc.obs.obs_id, chits_kvhc.obs.concept_id, chits_kvhc.obs.obs_datetime, chits_kvhc.person.person_id, chits_kvhc.person.birthdate FROM chits_kvhc.obs JOIN chits_kvhc.person ON chits_kvhc.obs.person_id = chits_kvhc.person.person_id WHERE chits_kvhc.obs.concept_id IN (7581,7582,7583,7584,7578,7579,7580,7859,20946,7585,7586,7587,20954,20957,20958,20948,20952))
      $this->db->query($create_view);
    }
    
    public function get_fully_immnunized_children($Start, $End, $Intervals, $Area){
      $this->load->database();
      $filter_birthday1 = "TIMESTAMPDIFF(YEAR, birthdate, '" . $Start."') BETWEEN '0' AND '100'";
      $filter_birthday2 = "TIMESTAMPDIFF(YEAR, birthdate, '" . $End."') BETWEEN '0' AND '100'";
      $fields;
      $grouping;
      if (strcmp($Intervals, "yearly") == 0){
        $fields = "COUNT(obs_id) AS Number, DATE_FORMAT(obs_datetime, '%Y') AS Period";
        $grouping = "YEAR(immunizations_view_" . $Area . ".obs_datetime)";
      }
      else if (strcmp($Intervals, "monthly") == 0 || strcmp($Intervals, "quarterly") == 0){
        if (strcmp($Intervals, "monthly") == 0){
          $fields = "COUNT(obs_id) AS Number, CONCAT(MONTHNAME(obs_datetime), ' ' ,DATE_FORMAT(obs_datetime, '%Y')) AS Period";
          $grouping = "YEAR(immunizations_view_" . $Area . ".obs_datetime), MONTH(immunizations_view_" . $Area . ".obs_datetime)";
        }
        else if (strcmp($Intervals, "quarterly") == 0) {
          $fields = "COUNT(obs_id) AS Number, CONCAT('Q', QUARTER(obs_datetime), ' ', YEAR(obs_datetime)) AS Period";
          $grouping = "YEAR(immunizations_view_" . $Area . ".obs_datetime), QUARTER(immunizations_view_" . $Area . ".obs_datetime)";
        }
      }
      $this->db->select($fields, FALSE)
      ->from("immunizations_view_" . $Area)
      ->group_start()
      ->where($filter_birthday1)
      ->or_group_start()
      ->where($filter_birthday2)
      ->group_end()
      ->group_end();
      $res = $this->db->get()->result();

      $ProperRes = $this->createNewRes_fully_immunized($res, $Start, $End, $Intervals);
      return $ProperRes;
    }

    private function createNewRes_fully_immunized($res, $Start, $End, $Intervals){
      $NewRes = array();
      $span = array();
      if($Intervals == "monthly"){
        $span = $this->getMonths($Start,$End);
      }else if($Intervals == "quarterly"){
        $span = $this->getQuarters($Start,$End);
      }else if($Intervals == "yearly"){
        $span = $this->getYears($Start, $End);
      }
      foreach ($span as $period) {
        $newPeriod = new stdClass();
        $flag = false;
        foreach($res as $row){
          $newPeriod->Period = $period;
          if(strcasecmp($row->Period, $period) == 0){
            @$newPeriod->Number = $row->Number;
            break;
          }
          else @$newPeriod->Number = "0";
        }
        array_push($NewRes, $newPeriod);
      }
            // $Span = 
      return $NewRes;
    }
    /** END OF FULLY IMMUNIZED MODEL FUNCTIONS =============================================================== */
  









   /**
    *  DEWORMING MODEL FUNCTIONS ================================================================================
    *  
    *   @author Virgilio M. Mendoza III <vmmendoza1@up.edu.ph>
    */

    public function set_view($Area){
      $this->load->database();
      $db_name = "chits_".$Area;
      $concept_id;

      if (strcmp($Area, "sipacalmacen") == 0) {
        $concept_id = "35";
      }
      else{
        $concept_id = "44";
      }
      $create_view = "CREATE OR REPLACE VIEW deworming_view_".$Area." AS (SELECT obs.obs_id, obs.obs_datetime, person.person_id, person.birthdate FROM ".$db_name.".obs JOIN ".$db_name.".person ON obs.person_id = person.person_id WHERE (TIMESTAMPDIFF(MONTH, birthdate, obs_datetime) BETWEEN 12 AND 59 AND obs.concept_id = ".$concept_id." AND obs.value_text LIKE '%deworming%'))";
      $this->db->query($create_view);
    }
   
    public function get_deworming2($Start, $End, $Intervals, $Area){
      $this->load->database();
      $fields;
      $grouping;
      $from = "deworming_view_".$Area;
      $filter3 = "obs_datetime BETWEEN '".$Start."' AND '".$End."'";

      if (strcmp($Intervals, "yearly") == 0){
        $fields = "COUNT(DISTINCT person_id) AS Number, DATE_FORMAT(obs_datetime, '%Y') AS Period";
        $grouping = "YEAR(obs_datetime)";
      }
      else if (strcmp($Intervals, "monthly") == 0 || strcmp($Intervals, "quarterly") == 0){
        if (strcmp($Intervals, "monthly") == 0){
          $fields = "COUNT(DISTINCT person_id) AS Number, CONCAT(MONTHNAME(obs_datetime), ' ' ,DATE_FORMAT(obs_datetime, '%Y')) AS Period";
          $grouping = "YEAR(obs_datetime), MONTH(obs_datetime)";
        }
        else if (strcmp($Intervals, "quarterly") == 0) {
          $fields = "COUNT(DISTINCT person_id) AS Number, CONCAT('Q', QUARTER(obs_datetime), ' ', YEAR(obs_datetime)) AS Period";
          $grouping = "YEAR(obs_datetime), QUARTER(obs_datetime)";
        }
      }

      $this->db->select($fields, FALSE)
      ->from($from)
      ->group_start()
      ->where($filter3)
      ->group_end()
      ->group_by($grouping);

      $res = $this->db->get()->result();

      if(empty($res)){
        $temp = new stdClass();
        $temp->Period = "";
        $temp->Number = "";
        array_push($res, $temp);
      }


      $ProperRes = $this->createNewRes_deworming($res, $Start, $End, $Intervals);

      return $ProperRes;
    }


    private function createNewRes_deworming($res, $Start, $End, $Intervals){
      $NewRes = array();
      $span = array();
      if($Intervals == "monthly"){
        $span = $this->getMonths($Start,$End);
      }else if($Intervals == "quarterly"){
        $span = $this->getQuarters($Start,$End);
      }else if($Intervals == "yearly"){
        $span = $this->getYears($Start, $End);
      }
      foreach ($span as $period) {
        $newPeriod = new stdClass();
        $flag = false;

        foreach($res as $row){
          $newPeriod->Period = $period;
          if(strcasecmp($row->Period, $period) == 0){
            @$newPeriod->Number = $row->Number;
            break;
          }
          else @$newPeriod->Number = "0";
        }
        array_push($NewRes, $newPeriod);
      }
      return $NewRes;
    }

  /**  END OF DEWORMING MODEL FUNCTIONS ========================================================================= */
   








  /**
   *  NUMBER OF PHILHEALTH BENEFICIARIES: DISAGGREGATED BY ROLE MODEL FUNCTIONS ============================
   *  @author John Robinson Francisco <jsfrancisco5@up.edu.ph>
   *  @author Sean Louie Pineda <pinedaseanlouie@gmai>
   */
  
  public function set_philhealth_role_view($Area='phasetwoareaone'){
    $this->load->database();
    $db_name = "chits_" . $Area;
    $create_view = "CREATE OR REPLACE VIEW philhealth_archaic_view_".$Area." AS (SELECT DISTINCT " . $db_name . ".person_attribute.person_id," . $db_name . ".person_attribute.date_created, value FROM " . $db_name . ".person_attribute WHERE " . $db_name . ".person_attribute.person_attribute_type_id = 18 and value = 16 or value = 17 or value = 18 or value = 19 or value = 299  or value = 7334 or value = 7335 or value = 7336 or value = 7337 or value = 8195 or value = 10008 GROUP BY value)";
    $this->db->query($create_view);
  }

  public function get_philhealth_archaic_role($Area,$Intervals,$Start, $End){
    $this->load->database();
    $db_name = "chits_" . $Area;
    $fields="";
    $grouping="";
     //   $this->db->db_select($Area);
    $this->db->select($fields, FALSE)
    ->from("philhealth_archaic_view_".$Area)
    ->where('date_created >=', $Start)
    ->where('date_created<=', $End);
    $res = $this->db->get()->result();      
    return $res;
  }



   /** END OF NUMBER OF PHILHEALTH BENEFICIARIES: DISAGGREGATED BY ROLE MODEL FUNCTIONS =================== */

  









  /** 
   *  EXCLUSIVE BREASTFEEDING MODEL FUNCTIONS =====================================================================
   *
   *  @author Reena Myka De Guzman <[<email address>]>
   */
   
    public function set_exclusive_breastfeeding_view($Area){
      $this->load->database();
      $db_name = "chits_" . $Area;
      $create_view = "CREATE OR REPLACE VIEW all_breastfeeding_view_" . $Area . " AS (SELECT obs.person_id, COUNT(obs.person_id) AS 'count_breastfeed_time'," . $db_name . ".person.birthdate FROM " . $db_name . ".obs JOIN " . $db_name . ".person ON " . $db_name . ".obs.person_id = " . $db_name. ".person.person_id WHERE (concept_id IN (1013, 1014, 1015, 1016, 1017, 1018)) GROUP BY person_id)";
      $this->db->query($create_view);
    }

    public function set_exclusive_breastfeeding_view_selected($Area){
       $this->load->database();
      $create_view = "CREATE OR REPLACE VIEW exclusivebreastfeeding_view_" . $Area . " AS (SELECT person_id, birthdate FROM all_breastfeeding_view_" . $Area . " WHERE `count_breastfeed_time` = 6)";
      $this->db->query($create_view);
    }

    public function get_exclusivebreastfeeding2($Start, $End, $Intervals, $Area){
       $this->load->database();
      $fields;
      $grouping;

      if (strcmp($Intervals, "yearly") == 0){
        $fields = "COUNT(person_id) AS Number, DATE_FORMAT(birthdate, '%Y') AS Period";
        $grouping = "YEAR(birthdate)";
      }
      else if (strcmp($Intervals, "monthly") == 0 || strcmp($Intervals, "Quarterly") == 0){
        if (strcmp($Intervals, "monthly") == 0){
          $fields = "COUNT(person_id) AS Number, DATE_FORMAT(birthdate, '%Y-%m') AS Period";
          $grouping = "YEAR(birthdate), MONTH(birthdate)";
        }
        else if (strcmp($Intervals, "quarterly") == 0) {
          $fields = "COUNT(person_id) AS Number, CONCAT(YEAR(birthdate), '-', QUARTER(birthdate)) AS Period";
          $grouping = "YEAR(birthdate), QUARTER(birthdate)";
        }
      }
      $query = "SELECT " . $fields . " FROM `exclusivebreastfeeding_view_" . $Area . "` GROUP BY " . $grouping;
      $q = $this->db->query($query);
      $res = $q->result();   

      // $ProperRes = $this->createNewRes_deworming($res, $Start, $End, $Intervals);

      return $res;   

    }

   
   /** END OF EXCLUSIVE BREASTFEEDING MODEL FUNCTIONS ============================================================ */









   /** 
    * AUXILLIARY MODEL FUNCTIONS ==============================================================================
    */
    

    public function compare_date($start_date,$end_date){
     if(strtotime($start_date) <= strtotime($end_date) ){
        return True;
     }

     return False;
    }
   
    public function getStartForQuarter($date){
      $year =  date("Y",strtotime($date));
      if(1 <= date("m",strtotime($date)) && date("m",strtotime($date)) <= 3){
        return $year."-01-01";
      }else if(4 <= date("m",strtotime($date)) && date("m",strtotime($date)) <= 6){
        return $year."-04-01";
      }else if(7 <= date("m",strtotime($date)) && date("m",strtotime($date)) <= 9){
        return $year."-07-01";
      }

      return $year."-10-01";
   }
   
   public function getQuarter($date){
      if(1 <= date("m",strtotime($date)) && date("m",strtotime($date)) <= 3){
        return 01;
      }else if(4 <= date("m",strtotime($date)) && date("m",strtotime($date)) <= 6){
        return 02;
      }else if(7 <= date("m",strtotime($date)) && date("m",strtotime($date)) <= 9){
        return 03;
      }

      return 04;
   }
   
   public function getMonthName($month){
      if($month=='01'){
        return "Jan";
      }else if($month=='02'){
        return "Feb";
      }else if($month=='03'){
        return "Mar";
      }else if($month=='04'){
        return "Apr";
      }else if($month=='05'){
        return "May";
      }else if($month=='06'){
        return "Jun";
      }else if($month=='07'){
        return "Jul";
      }else if($month=='08'){
        return "Aug";
      }else if($month=='09'){
        return "Sept";
      }else if($month=='10'){
        return "Oct";
      }else if($month=='11'){
        return "Nov";
      }else if($month=='12'){
        return "Dec";
      }
   }

    public function getMonths($Start, $End){
      $startD = new DateTime($Start);
      $endD = new DateTime($End);
      $nEndD = date_add($endD, date_interval_create_from_date_string('1 month'));
      $interval = DateInterval::createFromDateString('1 Month');
      $period   = new DatePeriod($startD, $interval, $nEndD);
      $span = array();

      foreach ($period as $dt) {
        $span[] = $dt->format('F Y');
      }


      return $span;
    }

    public function getQuarters($Start, $End){
      $startD = new DateTime($Start);
      $endD = new DateTime($End);
      $nEndD = date_add($endD, date_interval_create_from_date_string('1 month'));
      $interval = DateInterval::createFromDateString('1 Month');
      $period   = new DatePeriod($startD, $interval, $nEndD);
      $span = array();

      foreach ($period as $dt) {
        $temp = ceil((int)$dt->format('m')/3);
        if (!in_array("Q".$temp." ".$dt->format('Y'), $span)) {
          $span[] = "Q" . $temp . " " . $dt->format('Y');
        }
      }
      return $span;
    }
    
    
    public function getYears($Start, $End){
        $startD = new DateTime($Start);
        $endD = new DateTime($End);
        $nEndD = date_add($endD, date_interval_create_from_date_string('1 month'));
        $interval = DateInterval::createFromDateString('1 Year');
        $period   = new DatePeriod($startD, $interval, $nEndD);
        $span = array();

        foreach ($period as $dt) {
          $span[] = $dt->format('Y');
        }


        return $span;
    }
    
    public function populateWithZeros($span){
      $data = array();
      foreach ($span as $row) {
        $data[]= array('Period' => $row, 'Number' => 0);
      }
      return $data;
    }
    
    public function setvalues($empty, $values){
      $data = array();

            //TODO: bugged, shows only until last result of $values
      foreach ($empty as $eRow) {
        foreach ($values as $vRow) {
          if (strcmp($eRow['Period'], $vRow->Period) == 0) {
            $data[] = array('Period' => $eRow['Period'], 'Number' => $vRow->Number);
            unset($values[array_search($vRow, $values)]);
            break;
          }
          else{
            $data[] = array('Period' => $eRow['Period'], 'Number' => 0);
            break;
          }
        }
      }

      return $data;
    }

  /**  END OF AUXILLIARY FUNCTIONS ============================================================================= */






  






  /**
   *  UPDATE WAREHOUSE SECTION ==================================================================================
   *  @author Waleed C. Occidental <woccidental@up.edu.ph>
  */
  
   public function update_warehouse(){


        $query_birth_dates_others = "CREATE OR REPLACE VIEW birth_dates AS(SELECT obs_group_id, value_datetime AS birth_date, ((concept_id = 418 AND value_numeric IS NOT NULL) OR concept_id = 7512) AS has_month FROM obs WHERE concept_id IN (418, 7512) AND value_datetime IS NOT NULL)";
        $query_births_attended_others = "CREATE OR REPLACE VIEW births_attended AS(SELECT value_text AS profession, birth_date, has_month FROM obs INNER JOIN birth_dates ON obs.obs_group_id = birth_dates.obs_group_id WHERE concept_id = 429 AND value_coded IN (339, 340, 341, 8101, 20943) ORDER BY birth_date)";
        $query_birth_dates_sipacalmacen = "CREATE OR REPLACE VIEW birth_dates AS(SELECT obs_group_id, value_datetime AS birth_date, ((concept_id = 272 AND value_numeric IS NOT NULL) OR concept_id = 16346) AS has_month FROM obs WHERE concept_id IN (272, 17346) AND value_datetime IS NOT NULL)";
        $query_births_attended_sipacalmacen = "CREATE OR REPLACE VIEW births_attended AS( SELECT value_text AS profession, birth_date, has_month FROM obs INNER JOIN birth_dates ON obs.obs_group_id = birth_dates.obs_group_id WHERE concept_id = 191 AND value_coded IN (186, 187, 851, 16889) ORDER BY birth_date)";

        $query_livebirths_others = "CREATE OR REPLACE VIEW v_livebirths AS SELECT obs_id, obs_datetime AS birth_date, (concept_id = 9946) AS has_month FROM obs WHERE concept_id = 9946 AND obs_datetime IS NOT NULL UNION SELECT obs_id, value_datetime AS birth_date, ((concept_id = 418 AND value_numeric IS NOT NULL) OR concept_id = 7512) AS has_month FROM obs WHERE concept_id IN (418, 7512) AND value_datetime IS NOT NULL";
        $query_livebirths_sipacalmacen = "CREATE OR REPLACE VIEW v_livebirths AS SELECT obs_id, obs_datetime AS birth_date, (concept_id = 9946) AS has_month FROM obs WHERE concept_id = 17469 AND obs_datetime IS NOT NULL UNION SELECT obs_id, value_datetime AS birth_date, ((concept_id = 272 AND value_numeric IS NOT NULL) OR concept_id = 16346) AS has_month FROM obs WHERE concept_id IN (272, 17346) AND value_datetime IS NOT NULL";

        $query_contraceptive_others = "CREATE OR REPLACE VIEW v_status_family_planning_users AS (SELECT obs_id, person_id, value_text AS family_planning_status, obs_datetime FROM obs WHERE concept_id = 7978);";
        $query_contraceptive_sipacalmacen = "CREATE OR REPLACE VIEW v_status_family_planning_users AS (SELECT obs_id, person_id, value_text AS family_planning_status, obs_datetime FROM obs WHERE concept_id = 16784);";

        $query_population_warehouse = "CREATE OR REPLACE VIEW v_population AS (SELECT person_id, birthdate FROM person WHERE birthdate IS NOT NULL);";

        $query_philhealth_by_status = "CREATE OR REPLACE VIEW philhealth_expirationdates_person AS ( SELECT p_attr.person_id,p_attr.value, p_attr.date_created FROM person_attribute AS p_attr INNER JOIN person_attribute_type as p_attr_type ON p_attr.person_attribute_type_id = p_attr_type.person_attribute_type_id WHERE p_attr.person_attribute_type_id = 17);";

        $query_consultation = "CREATE OR REPLACE VIEW consultation AS ( SELECT * FROM patient  INNER JOIN encounter ON patient.patient_id = encounter.patient_id);";

        $this->load->database("kvhc");
        $this->db->query($query_birth_dates_others);
        $this->db->query($query_births_attended_others);
        $this->db->query($query_livebirths_others);
        $this->db->query($query_contraceptive_others);
        $this->db->query($query_population_warehouse);
        $this->db->query($query_philhealth_by_status);
        $this->db->query($query_consultation);
        $this->db->close();

        $this->load->database('phase2area1');
        $this->db->query($query_birth_dates_others);
        $this->db->query($query_births_attended_others);
        $this->db->query($query_livebirths_others);
        $this->db->query($query_contraceptive_others);
        $this->db->query($query_population_warehouse);
        $this->db->query($query_philhealth_by_status);
        $this->db->query($query_consultation);
        $this->db->close();

        $this->load->database('tangos');
        $this->db->query($query_birth_dates_others);
        $this->db->query($query_births_attended_others);
        $this->db->query($query_livebirths_others);
        $this->db->query($query_contraceptive_others);
        $this->db->query($query_population_warehouse);
        $this->db->query($query_philhealth_by_status);
        $this->db->query($query_consultation);
        $this->db->close();

        $this->load->database('sipacalmacen');
        $this->db->query($query_birth_dates_sipacalmacen);
        $this->db->query($query_births_attended_sipacalmacen);
        $this->db->query($query_livebirths_sipacalmacen);
        $this->db->query($query_contraceptive_sipacalmacen);
        $this->db->query($query_population_warehouse);
        $this->db->query($query_philhealth_by_status);
        $this->db->query($query_consultation);
        $this->db->close();
    }

    public function update_external_warehouse(){
        $areas = array("kvhc", "phase2area1", "sipacalmacen", "tangos");
        $queries = array();

        $this->load->database('warehouse');

        foreach($areas as $area){
            if(strcmp($area, "sipacalmacen") === 0){
                $queries["birth_dates"] = "CREATE OR REPLACE VIEW " . $area . "_v_birth_dates AS(SELECT obs_group_id, value_datetime AS birth_date, ((concept_id = 272 AND value_numeric IS NOT NULL) OR concept_id = 16346) AS has_month FROM chits_" . $area . ".obs WHERE concept_id IN (272, 17346) AND value_datetime IS NOT NULL)";

                $queries["births_attened"] = "CREATE OR REPLACE VIEW " . $area . "_v_births_attended AS( SELECT value_text AS profession, birth_date, has_month FROM chits_" . $area . ".obs AS obs INNER JOIN ". $area . "_v_birth_dates AS birth_dates ON obs.obs_group_id = birth_dates.obs_group_id WHERE concept_id = 191 AND value_coded IN (186, 187, 851, 16889) ORDER BY birth_date)";

                $queries["livebirths"] = "CREATE OR REPLACE VIEW " . $area . "_v_livebirths AS SELECT obs_id, obs_datetime AS birth_date, (concept_id = 9946) AS has_month FROM chits_" . $area . ".obs WHERE concept_id = 17469 AND obs_datetime IS NOT NULL UNION SELECT obs_id, value_datetime AS birth_date, ((concept_id = 272 AND value_numeric IS NOT NULL) OR concept_id = 16346) AS has_month FROM chits_" . $area . ".obs WHERE concept_id IN (272, 17346) AND value_datetime IS NOT NULL";

                $queries["contraceptives"] = "CREATE OR REPLACE VIEW " . $area . "_v_status_family_planning_users AS (SELECT obs_id, person_id, value_text AS family_planning_status, obs_datetime FROM chits_" . $area . ".obs WHERE concept_id = 16784);";
            }else{
                $queries["birth_dates"] = "CREATE OR REPLACE VIEW " . $area . "_v_birth_dates AS(SELECT obs_group_id, value_datetime AS birth_date, ((concept_id = 418 AND value_numeric IS NOT NULL) OR concept_id = 7512) AS has_month FROM chits_" . $area . ".obs WHERE concept_id IN (418, 7512) AND value_datetime IS NOT NULL)";

                $queries["births_attened"] = "CREATE OR REPLACE VIEW " . $area . "_v_births_attended AS(SELECT value_text AS profession, birth_date, has_month FROM chits_" . $area . ".obs AS obs INNER JOIN ". $area . "_v_birth_dates AS birth_dates ON obs.obs_group_id = birth_dates.obs_group_id WHERE concept_id = 429 AND value_coded IN (339, 340, 341, 8101, 20943) ORDER BY birth_date)";

                $queries["livebirths"] = "CREATE OR REPLACE VIEW " . $area . "_v_livebirths AS SELECT obs_id, obs_datetime AS birth_date, (concept_id = 9946) AS has_month FROM chits_" . $area . ".obs WHERE concept_id = 9946 AND obs_datetime IS NOT NULL UNION SELECT obs_id, value_datetime AS birth_date, ((concept_id = 418 AND value_numeric IS NOT NULL) OR concept_id = 7512) AS has_month FROM chits_" . $area . ".obs WHERE concept_id IN (418, 7512) AND value_datetime IS NOT NULL";

                $queries["contraceptives"] = "CREATE OR REPLACE VIEW " . $area . "_v_status_family_planning_users AS (SELECT obs_id, person_id, value_text AS family_planning_status, obs_datetime FROM chits_" . $area . ".obs WHERE concept_id = 7978);";
            }

            $queries["population"] = "CREATE OR REPLACE VIEW " . $area . "_v_population AS (SELECT person_id, birthdate FROM chits_" . $area . ".person WHERE birthdate IS NOT NULL);";

            $queries["philhealth"] = "CREATE OR REPLACE VIEW " . $area . "_v_philhealth_expiration_dates_person AS ( SELECT p_attr.person_id,p_attr.value, p_attr.date_created FROM chits_" . $area . ".person_attribute AS p_attr INNER JOIN chits_" . $area . ".person_attribute_type as p_attr_type ON p_attr.person_attribute_type_id = p_attr_type.person_attribute_type_id WHERE p_attr.person_attribute_type_id = 17);";
            
            $queries["consultation"] = "CREATE OR REPLACE VIEW " . $area  . "_consultation AS ( SELECT patient.patient_id, encounter.encounter_id, encounter.encounter_type, encounter_type.name, encounter_type.description, encounter.encounter_datetime FROM chits_" . $area . ".patient AS patient INNER JOIN chits_" . $area . ".encounter AS encounter ON patient.patient_id = encounter.patient_id INNER JOIN chits_" . $area . ".encounter_type AS encounter_type ON encounter.encounter_type = encounter_type.encounter_type_id );";

            foreach ($queries as $query) {
                $this->db->query($query);
            }
        }
    }


}
