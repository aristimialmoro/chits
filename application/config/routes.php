<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'data';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['consultation'] = 'data/consultation';
$route['disaggregated_births'] = 'data/disaggregated_births';
$route['population_by_age_bracket'] = 'data/population_by_age_bracket';

$route['births'] = 'data/births';
$route['births/(:any)'] = 'data/births/$1';
$route['births/(:any)/(:any)'] = 'data/births/$1/$2';
$route['births/(:any)/(:any)/(:any)/(:any)'] = 'data/births/$1/$2/$3/$4';

$route['births_table'] = 'data/births_table';
$route['births_table/(:any)'] = 'data/births_table/$1';
$route['births_table/(:any)/(:any)'] = 'data/births_table/$1/$2';
$route['births_table/(:any)/(:any)/(:any)/(:any)'] = 'data/births_table/$1/$2/$3/$4';

$route['births_table_csv'] = 'data/births_table_csv';
$route['births_table_csv/(:any)'] = 'data/births_table_csv/$1';
$route['births_table_csv/(:any)/(:any)'] = 'data/births_table_csv/$1/$2';
$route['births_table_csv/(:any)/(:any)/(:any)/(:any)'] = 'data/births_table_csv/$1/$2/$3/$4';

$route['philhealth_by_status'] = 'data/philhealth_by_status';
$route['philhealth_by_status/(:any)'] = 'data/philhealth_by_status/$1';
$route['philhealth_by_status/(:any)/(:any)'] = 'data/philhealth_by_status/$1/$2';
$route['philhealth_by_status/(:any)/(:any)/(:any)/(:any)'] = 'data/philhealth_by_status/$1/$2/$3/$4';

$route['philhealth_by_status_table'] = 'data/philhealth_by_status_table';
$route['philhealth_by_status_table/(:any)'] = 'data/philhealth_by_status_table/$1';
$route['philhealth_by_status_table/(:any)/(:any)'] = 'data/philhealth_by_status_table/$1/$2';
$route['philhealth_by_status_table/(:any)/(:any)/(:any)/(:any)'] = 'data/philhealth_by_status_table/$1/$2/$3/$4';

$route['philhealth_by_status_table_csv'] = 'data/philhealth_by_status_table_csv';
$route['philhealth_by_status_table_csv/(:any)'] = 'data/philhealth_by_status_table_csv/$1';
$route['philhealth_by_status_table_csv/(:any)/(:any)'] = 'data/philhealth_by_status_table_csv/$1/$2';
$route['philhealth_by_status_table_csv/(:any)/(:any)/(:any)/(:any)'] = 'data/philhealth_by_status_table_csv/$1/$2/$3/$4';

$route['family_planning_status'] = 'data/contraceptive';
$route['family_planning_status/(:any)'] = 'data/contraceptive/$1';
$route['family_planning_status/(:any)/(:any)'] = 'data/contraceptive/$1/$2';
$route['family_planning_status/(:any)/(:any)/(:any)/(:any)'] = 'data/contraceptive/$1/$2/$3/$4';

$route['family_planning_status_table'] = 'data/contraceptive_table';
$route['family_planning_status_table/(:any)'] = 'data/contraceptive_table/$1';
$route['family_planning_status_table/(:any)/(:any)'] = 'data/contraceptive_table/$1/$2';
$route['family_planning_status_table/(:any)/(:any)/(:any)/(:any)'] = 'data/contraceptive_table/$1/$2/$3/$4';

$route['birth_rate'] = 'data/birth_rate';
$route['birth_rate/(:any)'] = 'data/birth_rate/$1';
$route['birth_rate/(:any)/(:any)'] = 'data/birth_rate/$1/$2';
$route['birth_rate/(:any)/(:any)/(:any)/(:any)'] = 'data/birth_rate/$1/$2/$3/$4';

$route['birth_rate_table'] = 'data/birth_rate_table';
$route['birth_rate_table/(:any)'] = 'data/birth_rate_table/$1';
$route['birth_rate_table/(:any)/(:any)'] = 'data/birth_rate_table/$1/$2';
$route['birth_rate_table/(:any)/(:any)/(:any)/(:any)'] = 'data/birth_rate_table/$1/$2/$3/$4';

$route['birth_rate_csv'] = 'data/birth_rate_csv';
$route['birth_rate_csv/(:any)'] = 'data/birth_rate_csv/$1';
$route['birth_rate_csv/(:any)/(:any)'] = 'data/birth_rate_csv/$1/$2';
$route['birth_rate_csv/(:any)/(:any)/(:any)/(:any)'] = 'data/birth_rate_csv/$1/$2/$3/$4';

$route['deworming'] = 'data/deworming';
$route['deworming/(:any)'] = 'data/deworming/$1';
$route['deworming/(:any)/(:any)'] = 'data/deworming/$1/$2';
$route['deworming/(:any)/(:any)/(:any)/(:any)'] = 'data/deworming/$1/$2/$3/$4';

$route['exclusive_breastfeeding'] = 'data/exclusive_breastfeeding';
$route['exclusive_breastfeeding/(:any)'] = 'data/exclusive_breastfeeding/$1';
$route['exclusive_breastfeeding/(:any)/(:any)'] = 'data/exclusive_breastfeeding/$1/$2';
$route['exclusive_breastfeeding/(:any)/(:any)/(:any)/(:any)'] = 'data/exclusive_breastfeeding/$1/$2/$3/$4';

$route['fully_immunized'] = 'data/fully_immunized';
$route['fully_immunized/(:any)'] = 'data/fully_immunized/$1';
$route['fully_immunized/(:any)/(:any)'] = 'data/fully_immunized/$1/$2';
$route['fully_immunized/(:any)/(:any)/(:any)/(:any)'] = 'data/fully_immunized/$1/$2/$3/$4';

$route['philhealth_archaic'] = 'data/philhealth_archaic';
$route['philhealth_archaic/(:any)'] = 'data/philhealth_archaic/$1';
$route['philhealth_archaic/(:any)/(:any)'] = 'data/philhealth_archaic/$1/$2';
$route['philhealth_archaic/(:any)/(:any)/(:any)/(:any)'] = 'data/philhealth_archaic/$1/$2/$3/$4';



//$route['(:any)'] = 'pages/data/$1';
